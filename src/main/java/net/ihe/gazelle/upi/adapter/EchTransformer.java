package net.ihe.gazelle.upi.adapter;

import net.ihe.gazelle.upi.model.ech_0007.SwissMunicipalityType;
import net.ihe.gazelle.upi.model.ech_0008.CountryType;
import net.ihe.gazelle.upi.model.ech_0011.GeneralPlaceType;
import net.ihe.gazelle.upi.model.ech_0011.NationalityDataType;
import net.ihe.gazelle.upi.model.ech_0021.NameOfParentType;
import net.ihe.gazelle.upi.model.ech_0044.DatePartiallyKnownType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsFromUPIType;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class EchTransformer {

    public EchTransformer() {

    }

    private String getValueOfCell(Cell cell) {
        String cellValue = "";
        if (cell != null && cell.getCellType() != null) {
            switch (cell.getCellType()) {
                case STRING:
                    cellValue = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    cellValue = BigDecimal.valueOf(cell.getNumericCellValue()).toBigInteger().toString();
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case BLANK:
                default:
            }
        }
        return cellValue;
    }

    private Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public DatePartiallyKnownType getDatePartiallyKnownFromCell(Cell cell) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlGregCal = getXMLGregorianCalendarFromCell(cell);
        DatePartiallyKnownType datePartiallyKnownType = new DatePartiallyKnownType();
        datePartiallyKnownType.setYearMonthDay(xmlGregCal);
        datePartiallyKnownType.setYear(xmlGregCal);
        datePartiallyKnownType.setYearMonth(xmlGregCal);
        return datePartiallyKnownType;
    }

    public DatePartiallyKnownType getDateOfBirthFromRow(Row row) throws DatatypeConfigurationException {
        return getDatePartiallyKnownFromCell(row.getCell(4));
    }

    public XMLGregorianCalendar getDateOfDeathFromRow(Row row) throws DatatypeConfigurationException {
        return getXMLGregorianCalendarFromCell(row.getCell(6));
    }

    private XMLGregorianCalendar createXMLGregorianCalendarFromString(String date) throws DatatypeConfigurationException {
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
    }

    public XMLGregorianCalendar getXMLGregorianCalendarFromCell(Cell cell) throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        if (cell != null && cell.getCellType() != null && cell.getCellType().equals(CellType.NUMERIC)) {
            try {
                LocalDate localDate2 = LocalDate.of(1899, Month.DECEMBER, 30).plusDays(Long.parseLong(getValueOfCell(cell)));
                Date date = convertToDateViaInstant(localDate2);
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(date);
                // +1 for month because the factory starts at 0. I.e january = 0 and december = 11
                xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
            } catch (DatatypeConfigurationException e) {
                throw e;
            }
        }
        return xmlGregCal;
    }

    public List<String> getSpidsFromRow(Row row) {
        List<String> spids = new ArrayList<>();
        for (int i = 1; row.getLastCellNum() > 0 && i < row.getLastCellNum(); i++) {
            spids.add(getValueOfCell(row.getCell(i)));
        }
        return spids;
    }

    public List<NameOfParentType> getMotherNames(Row row) {
        NameOfParentType motherName = new NameOfParentType();
        motherName.setFirstName(getValueOfCell(row.getCell(11)));
        motherName.setOfficialName(getValueOfCell(row.getCell(12)));
        List<NameOfParentType> motherNames = new ArrayList<>();
        motherNames.add(motherName);
        return motherNames;
    }

    public List<NameOfParentType> getFatherNames(Row row) {
        NameOfParentType fatherName = new NameOfParentType();
        fatherName.setFirstName(getValueOfCell(row.getCell(13)));
        fatherName.setOfficialName(getValueOfCell(row.getCell(14)));
        List<NameOfParentType> fatherNames = new ArrayList<>();
        fatherNames.add(fatherName);
        return fatherNames;
    }

    public PersonFromUPIType getPersonFromUpiFromRow(Row row) {
        PersonFromUPIType personFromUPIType = new PersonFromUPIType();
        try {
            String dateNow = ZonedDateTime.now(ZoneId.of("Europe/Zurich")).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "Z";
            XMLGregorianCalendar xmlGregorianCalendar = createXMLGregorianCalendarFromString(dateNow);
            personFromUPIType.setRecordTimestamp(xmlGregorianCalendar);

            personFromUPIType.setDateOfBirth(getDateOfBirthFromRow(row));
            personFromUPIType.setDateOfDeath(getDateOfDeathFromRow(row));

            if (StringUtils.isNotBlank(getValueOfCell(row.getCell(6)))) {
                personFromUPIType.setDateOfDeath(getXMLGregorianCalendarFromCell(row.getCell(6)));
            }

            personFromUPIType.setFirstName(getValueOfCell(row.getCell(7)));
            personFromUPIType.setOfficialName(getValueOfCell(row.getCell(8)));
            personFromUPIType.setOriginalName(getValueOfCell(row.getCell(9)));
            personFromUPIType.setSex(getValueOfCell(row.getCell(10)));

            List<NameOfParentType> motherNames = getMotherNames(row);
            List<NameOfParentType> fatherNames = getFatherNames(row);
            personFromUPIType.setMothersName(motherNames);
            personFromUPIType.setFathersName(fatherNames);

            personFromUPIType.setNationalityData(getNationalityData(row));

            personFromUPIType.setPlaceOfBirth(getPlaceOfBirth(row));

            return personFromUPIType;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return personFromUPIType;
    }

    public NationalityDataType getNationalityData(Row row) {
        List<NationalityDataType.CountryInfo> countryInfos = new ArrayList<>();
        CountryType countryType = new CountryType();
        if (StringUtils.isAlpha(getValueOfCell(row.getCell(16)))) {
            countryType.setCountryNameShort("UNKNOWN");
        } else {
            countryType.setCountryId(Integer.valueOf(getValueOfCell(row.getCell(16))));

            switch (countryType.getCountryId()) {
                case 8100:
                    countryType.setCountryNameShort("SUISSE");
                    break;
                case 8212:
                    countryType.setCountryNameShort("FRANCE");
                    break;
                case 8207:
                    countryType.setCountryNameShort("ALLEMAGNE");
                    break;
                case 8218:
                    countryType.setCountryNameShort("ITALIE");
                    break;
                case 8215:
                    countryType.setCountryNameShort("ANGLETERRE");
                    break;
                default:
                    countryType.setCountryNameShort("UNKNOWN");
            }
        }
        NationalityDataType.CountryInfo countryInfo = new NationalityDataType.CountryInfo();
        countryInfo.setCountry(countryType);
        countryInfos.add(countryInfo);
        NationalityDataType nationalityDataType = new NationalityDataType();
        nationalityDataType.setNationalityStatus("2");
        nationalityDataType.setCountryInfo(countryInfos);
        return nationalityDataType;
    }

    public GeneralPlaceType getPlaceOfBirth(Row row) {
        String cellValue = getValueOfCell(row.getCell(17));
        if (StringUtils.isBlank(cellValue)) {
            return null;
        }
        SwissMunicipalityType swissMunicipalityType = new SwissMunicipalityType();
        swissMunicipalityType.setMunicipalityName(cellValue);
        if (isNumeric(cellValue)) {
            swissMunicipalityType.setMunicipalityId(Integer.valueOf(cellValue));
            swissMunicipalityType.setHistoryMunicipalityId(Integer.valueOf(cellValue));
        }
        GeneralPlaceType generalPlaceType = new GeneralPlaceType();
        generalPlaceType.setSwissTown(swissMunicipalityType);
        return generalPlaceType;
    }

    private boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }


    public Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate rowToCandidate(Row person, List<String> spids) {
        Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate candidate = new Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate();
        PersonFromUPIType personFromUPIType = getPersonFromUpiFromRow(person);
        PidsFromUPIType pidsFromUPIType = getPidsFromUPITypeFromRow(person, spids);

        candidate.setPids(pidsFromUPIType);
        candidate.setPersonFromUPI(personFromUPIType);
        return candidate;
    }

    public Response.PositiveResponse.SearchPersonResponse.Found rowToFoundSearchPersonResponse(Row person, List<String> spids) {
        PersonFromUPIType personFromUPIType = getPersonFromUpiFromRow(person);
        PidsFromUPIType pidsFromUPIType = getPidsFromUPITypeFromRow(person, spids);

        Response.PositiveResponse.SearchPersonResponse.Found found = new Response.PositiveResponse.SearchPersonResponse.Found();
        found.setPids(pidsFromUPIType);
        found.setPersonFromUPI(personFromUPIType);

        return found;
    }


    public Response.PositiveResponse.GetInfoPersonResponse rowToInfoPersonResponse(Row person, List<String> spids) {
        PersonFromUPIType personFromUPIType = getPersonFromUpiFromRow(person);
        PidsFromUPIType pidsFromUPIType = getPidsFromUPITypeFromRow(person, spids);

        Response.PositiveResponse.GetInfoPersonResponse getInfoPersonResponse = new Response.PositiveResponse.GetInfoPersonResponse();
        getInfoPersonResponse.setPersonFromUPI(personFromUPIType);
        getInfoPersonResponse.setPids(pidsFromUPIType);

        return getInfoPersonResponse;
    }

    private PidsFromUPIType getPidsFromUPITypeFromRow(Row row, List<String> spids) {
        PidsFromUPIType pidsFromUPIType = new PidsFromUPIType();
        pidsFromUPIType.setVn(Long.valueOf(getValueOfCell(row.getCell(0))));
        pidsFromUPIType.setSpid(spids);
        return pidsFromUPIType;
    }

}
