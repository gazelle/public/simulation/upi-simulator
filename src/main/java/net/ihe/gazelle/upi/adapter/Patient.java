package net.ihe.gazelle.upi.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.upi.exception.InvalidParameterException;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.ech_0044.DatePartiallyKnownType;
import net.ihe.gazelle.upi.model.ech_0213_commons.NegativeReportType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsFromUPIType;
import net.ihe.gazelle.upi.model.ech_0214.Notice;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.ihe.gazelle.upi.adapter.ColumnName.*;

public class Patient implements PatientDAO {

    private final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(Patient.class);
    private final String PATH_TO_PATIENT_DATA = "WEB-INF/patient/patient_data.xlsx";
    private XSSFWorkbook workbook;

    private final String SHEET_PERSONS = "persons";
    private final String SHEET_CANCELED = "canceled";
    private final String SHEET_SEARCH = "search";
    private final String SHEET_INACTIVE = "inactive";
    private final String SHEET_ACTIVE = "active";
    private final String SHEET_ZERO_EPR_SPID = "0 EPR-SPID";
    private final String SHEET_ONE_EPR_SPID = "1 EPR-SPID";
    private final String SHEET_TWO_EPR_SPID = "2 EPR-SPID";

    public Patient() throws NotFoundException {
        try {
            InputStream inputStream =
                    this.getClass().getClassLoader().getResourceAsStream(PATH_TO_PATIENT_DATA);
            workbook = new XSSFWorkbook(inputStream);

        } catch (FileNotFoundException e) {
            throw new NotFoundException("Patient data file has not been found", e);
        } catch (IOException e) {
            throw new NotFoundException(e);
        }
    }

    @Override
    public Response.PositiveResponse.CompareDataResponse getCompareDataResponseFromDataRequest(Request.Content.CompareDataRequest compareDataRequest) throws MultipleFoundException, NotFoundException {
        Response.PositiveResponse.CompareDataResponse compareDataResponse = initializeCompareDataResponse(compareDataRequest);
        long vnLong = compareDataRequest.getPids().getVn();
        String vn = String.valueOf(vnLong);
        String spid = compareDataRequest.getPids().getSPID();

        if (existsActiveVN(vn)) {
            List<String> spids = getSpidsFromVn(vn);
            if (spids.contains(spid)) {
                compareDataResponse.setIdenticalData("identical");
            } else {
                Response.PositiveResponse.CompareDataResponse.DifferentData differentData = new Response.PositiveResponse.CompareDataResponse.DifferentData();
                PidsFromUPIType pidsFromUPIType = new PidsFromUPIType();
                pidsFromUPIType.setVn(vnLong);
                pidsFromUPIType.setSpid(spids);
                differentData.setPids(pidsFromUPIType);
                compareDataResponse.setDifferentData(differentData);
            }
        } else if (existsActiveSPID(spid)) {
            List<Row> persons = getPersonFromSpid(spid);
            if (persons.size() > 1) {
                NegativeReportType negativeReportType = new NegativeReportType();
                NegativeReportType.Notice notice = new NegativeReportType.Notice();
                notice.setCode(300201);
                notice.setDescriptionLanguage("FR");
                notice.setCodeDescription("Plusieurs personnes correspondent");
                negativeReportType.setNotice(notice);
                compareDataResponse.setNegativReportOnCompareData(negativeReportType);
                compareDataResponse.setEchoPidsRequest(null);
            } else if (persons.size() == 1) {
                String currentVn = getValueOfCell(persons.get(0).getCell(0));
                List<String> currentSpid = new ArrayList<>();
                currentSpid.add(spid);
                Response.PositiveResponse.CompareDataResponse.DifferentData differentData = new Response.PositiveResponse.CompareDataResponse.DifferentData();
                PidsFromUPIType pidsFromUPIType = new PidsFromUPIType();
                pidsFromUPIType.setVn(Long.valueOf(currentVn));
                pidsFromUPIType.setSpid(currentSpid);
                differentData.setPids(pidsFromUPIType);
                compareDataResponse.setDifferentData(differentData);
            }

        } else {
            NegativeReportType negativeReportType = new NegativeReportType();
            NegativeReportType.Notice notice = new NegativeReportType.Notice();
            notice.setCode(300201);
            notice.setDescriptionLanguage("FR");
            notice.setCodeDescription("Le NAVS n'existe pas");
            negativeReportType.setNotice(notice);
            compareDataResponse.setNegativReportOnCompareData(negativeReportType);
            compareDataResponse.setEchoPidsRequest(null);
        }
        return compareDataResponse;
    }

    @Override
    public PersonFromUPIType getPersonFromUpiTypeFromSpid(String spid) throws NotFoundException, MultipleFoundException {
        String vn = getVnFromSpid(spid);
        List<Row> persons = getPersonFromVn(vn);
        return new EchTransformer().getPersonFromUpiFromRow(persons.get(0));
    }

    @Override
    public PersonFromUPIType getPersonFromUPIFromeCH0213Request(net.ihe.gazelle.upi.model.ech_0213.Request req) {
        try {
            String pid = req.getContent().getPidsToUPI().get(0).getContent().get(0).getValue().toString();

            if (pid != null) {

                if (13 == pid.length() && pid.startsWith("7560")) {
                    return getInfoPersonFromVN(pid).getPersonFromUPI();
                } else {
                    return getPersonFromUpiTypeFromSpid(pid);
                }

            } else return null;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean existsActiveSPID(String spid) {
        for (int i = 1; i < 7; i++) {
            if (isParamExistingInColumnInSheet(spid, COLUMN_EPR_SPID_INDEX + i, SHEET_ACTIVE)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Response.PositiveResponse.GetInfoPersonResponse getInfoPersonFromPersonRequest(Request.Content.GetInfoPersonRequest getInfoPersonRequest) throws NotFoundException, InvalidParameterException, MultipleFoundException {

        String vn = null;
        String spid = null;

        if (getInfoPersonRequest.getPid().getVn() != null) {
            vn = String.valueOf(getInfoPersonRequest.getPid().getVn());
        }
        if (getInfoPersonRequest.getPid().getSPID() != null) {
            spid = getInfoPersonRequest.getPid().getSPID();
        }

        if (StringUtils.isNotBlank(vn)) {
            return getInfoPersonFromVN(vn);
        } else if (StringUtils.isNotBlank(spid)) {
            return getInfoPersonFromSPID(spid);
        } else {
            throw new InvalidParameterException("Aucun NAVS ou SPID spécifié dans la requête");
        }
    }

    @Override
    public Response.PositiveResponse.SearchPersonResponse getSearchPersonResponseFromSearchPersonRequest(Request.Content.SearchPersonRequest searchPersonRequest) throws InvalidParameterException {
        checkSearchPersonRequestValid(searchPersonRequest);

        Set<Row> rows;
        try {
            rows = getMatchingRows(searchPersonRequest);
        } catch (DatatypeConfigurationException e) {
            rows = new HashSet<>();
        }

        return updateSearchPersonResponseWithPatientData(rows, searchPersonRequest);
    }

    private Response.PositiveResponse.SearchPersonResponse updateSearchPersonResponseWithPatientData(Set<Row> rows, Request.Content.SearchPersonRequest searchPersonRequest) {
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse = initializeSearchPersonResponse(searchPersonRequest);

        switch (rows.size()) {
            case 0:
                searchPersonResponse.setNotFound("notFound");
                break;
            case 1:
                Row row = rows.iterator().next();
                String vn = getVnFromRow(row);
                List<String> spids = getSpidsFromVn(vn);
                Response.PositiveResponse.SearchPersonResponse.Found found = new EchTransformer().rowToFoundSearchPersonResponse(row, spids);
                searchPersonResponse.setFound(found);
                break;
            default:
                List<Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate> candidates = new ArrayList<>();
                for (Row rowMaybeFound : rows) {
                    String vnMaybeFound = getVnFromRow(rowMaybeFound);
                    List<String> spidsMaybeFound = getSpidsFromVn(vnMaybeFound);
                    candidates.add(new EchTransformer().rowToCandidate(rowMaybeFound, spidsMaybeFound));
                }
                Response.PositiveResponse.SearchPersonResponse.MaybeFound maybeFound = new Response.PositiveResponse.SearchPersonResponse.MaybeFound();
                maybeFound.setCandidate(candidates);
                searchPersonResponse.setMaybeFound(maybeFound);
                break;
        }
        return searchPersonResponse;
    }

    private boolean areEqualsDateOfBirth(DatePartiallyKnownType date1, DatePartiallyKnownType date2) {
        if (isDateValid(date1) && isDateValid(date2)) {
            return date1.getYearMonthDay().getYear() == date2.getYearMonthDay().getYear() &&
                    date1.getYearMonthDay().getMonth() == date2.getYearMonthDay().getMonth() &&
                    date1.getYearMonthDay().getDay() == date2.getYearMonthDay().getDay();
        }
        return false;
    }

    private boolean isDateValid(DatePartiallyKnownType date) {
        return date != null && date.getYearMonthDay() != null;
    }

    private List<Row> searchParamInSheet(String param, String columnName, String sheetName) {
        List<Row> rows = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheet(sheetName);
        try {
            int columnIndex = getIndexOfColumn(sheet, columnName);

            for (Row row : sheet) {
                Cell cell = row.getCell(columnIndex);
                if (!isCellEmpty(cell)) {
                    String cellValue = getValueOfCell(cell);
                    if (isMatchingRowDependingOfComparisonMethod(param, cellValue, columnName)) {
                        rows.add(row);
                    }
                }
            }
        } catch (NotFoundException ignored) {

        }
        return rows;
    }

    private boolean isMatchingRowDependingOfComparisonMethod(String param, String cellValue, String column) {
        boolean result = false;
        switch (column) {
            case COLUMN_SSN:
            case COLUMN_SSN_STATUS:
            case COLUMN_DATE_OF_BIRTH:
            case COLUMN_EPR_SPID_CANCELED:
            case COLUMN_EPR_SPID_ACTIVE:
            case COLUMN_EPR_SPID_INACTIVE:
            case COLUMN_ONE_EPR_SPID_ACTIVE:
            case COLUMN_TWO_EPR_SPID_ACTIVE:
            case COLUMN_ONE_EPR_SPID_INACTIVE:
            case COLUMN_TWO_EPR_SPID_INACTIVE:
            case COLUMN_EPR_SPID_INDEX:
            case COLUMN_EPR_SPID_INDEX_ONE:
            case COLUMN_EPR_SPID_INDEX_TWO:
            case COLUMN_EPR_SPID_INDEX_THREE:
            case COLUMN_EPR_SPID_INDEX_FOUR:
            case COLUMN_EPR_SPID_INDEX_FIVE:
            case COLUMN_EPR_SPID_INDEX_SIX:
                if (StringUtils.isNotBlank(cellValue)) {
                    if ((cellValue.equalsIgnoreCase(param))) {
                        result = true;
                    }
                }
                break;
            default:
                if (StringUtils.isNotBlank(cellValue)) {
                    if (StringUtils.containsIgnoreCase(cellValue, param)) {
                        result = true;
                    }
                }
        }
        return result;
    }

    private int getIndexOfColumn(XSSFSheet sheet, String columnName) throws NotFoundException {
        for (Row row : sheet) {
            for (Cell cell : row) {
                if (cell != null) {
                    String cellValue = getValueOfCell(cell);
                    if (columnName.contains(cellValue)) {
                        return cell.getColumnIndex();
                    }
                }
            }
        }
        throw new NotFoundException("The column name " + columnName + " was not found");
    }

    private String getValueOfCell(Cell cell) {
        String cellValue = "";
        if (cell != null && cell.getCellType() != null) {
            switch (cell.getCellType()) {
                case BOOLEAN:
                    cellValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case NUMERIC:
                    cellValue = BigDecimal.valueOf(cell.getNumericCellValue()).toBigInteger().toString();
                    break;
                case STRING:
                    cellValue = cell.getStringCellValue();
                    break;
                case BLANK:
                default:
            }
        }
        return cellValue;
    }

    private boolean isCellEmpty(final Cell cell) {
        if (cell == null) {
            return true;
        }
        if (cell.getCellType() == CellType.BLANK) {
            return true;
        }
        return cell.getCellType() == CellType.STRING && cell.getStringCellValue().trim().isEmpty();
    }

    @Override
    public List<String> getSpidsFromVn(String vn) {
        List<String> spids = new ArrayList<>();
        try {
            List<Row> rows = searchParamInSheet(vn, COLUMN_SSN, SHEET_ACTIVE);
            Row firstRow = rows.get(0);
            for (int i = 1; firstRow.getLastCellNum() > 0 && i < firstRow.getLastCellNum(); i++) {
                spids.add(getValueOfCell(firstRow.getCell(i)));
            }
        } catch (IndexOutOfBoundsException ignored) {
            //Ignored : this means that the navs has no associated spid. It can happen
        }
        return spids;
    }

    private boolean isParamExistingInColumnInSheet(String param, String columnName, String sheet) {
        List<Row> rows = searchParamInSheet(param, columnName, sheet);
        return rows.size() == 1;
    }

    private boolean existsVN(String vn) {
        return isParamExistingInColumnInSheet(vn, COLUMN_SSN, SHEET_ACTIVE) ||
                isParamExistingInColumnInSheet(vn, COLUMN_SSN, SHEET_INACTIVE) ||
                isParamExistingInColumnInSheet(vn, COLUMN_SSN, SHEET_CANCELED) ||
                isParamExistingInColumnInSheet(vn, COLUMN_SSN, SHEET_PERSONS);
    }

    private boolean existsActiveVN(String vn) throws MultipleFoundException {
        if (existsVN(vn)) {
            try {
                List<Row> rows = searchParamInSheet(vn, COLUMN_SSN, SHEET_CANCELED);
                if (rows.size() > 1) {
                    throw new MultipleFoundException("Multiple NAVS found");
                }
                Row firstRow = rows.get(0);
                String cellValue = getValueOfCell(firstRow.getCell(1));
                return !cellValue.equals("inactive");
            } catch (IndexOutOfBoundsException outOfBoundsException) {
                //It means the navs is not inactive
                return true;
            }
        }
        return false;
    }

    @Override
    public String getVnFromSpid(String spid) throws NotFoundException, MultipleFoundException {
        List<Row> persons = getPersonFromSpid(spid);
        if (persons.isEmpty()) {
            throw new NotFoundException("Ce SPID n'a pas de NAVS actif");
        }
        if (persons.size() == 1) {
            String vn = getValueOfCell(persons.get(0).getCell(0));
            return vn;
        } else {
            throw new MultipleFoundException("Cet SPID est utilisé par plusieurs personnes");
        }
    }


    private Response.PositiveResponse.GetInfoPersonResponse getInfoPersonFromSPID(String spid) throws NotFoundException, MultipleFoundException {
        String vn = getVnFromSpid(spid);
        return getInfoPersonFromVN(vn);
    }

    private List<Row> getPersonFromSpid(String spid) {
        List<Row> persons = new ArrayList<>();
        for (int i = 1; i < 7; i++) {
            persons.addAll(searchParamInSheet(spid, COLUMN_EPR_SPID_INDEX + i, SHEET_ACTIVE));
        }
        return persons;
    }

    private List<Row> getPersonFromVn(String vn) throws MultipleFoundException, NotFoundException {
        if (existsActiveVN(vn)) {
            List<Row> persons = searchParamInSheet(vn, COLUMN_SSN, SHEET_PERSONS);
            if (persons.isEmpty()) {
                throw new NotFoundException("Ce NAVS n'est pas actif");
            } else if (persons.size() > 1) {
                throw new MultipleFoundException("Ce NAVS est utilisé par plusieurs personnes");
            }
            return persons;
        }
        throw new NotFoundException("Ce NAVS n'est associé à personne");
    }

    private Response.PositiveResponse.GetInfoPersonResponse getInfoPersonFromVN(String vn) throws NotFoundException, MultipleFoundException {
        List<Row> persons = getPersonFromVn(vn);
        List<String> spids = getSpidsFromVn(vn);
        return new EchTransformer().rowToInfoPersonResponse(persons.get(0), spids);
    }

    private Response.PositiveResponse.SearchPersonResponse initializeSearchPersonResponse(Request.Content.SearchPersonRequest searchPersonRequest) {
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse = new Response.PositiveResponse.SearchPersonResponse();
        Notice notice = new Notice();
        notice.setDescriptionLanguage("FR");
        List<Notice> notices = new ArrayList<>();
        notices.add(notice);
        searchPersonResponse.setSearchPersonRequestId(searchPersonRequest.getSearchPersonRequestId());
        searchPersonResponse.setAlgorithm(searchPersonRequest.getAlgorithm());
        searchPersonResponse.setNotice(notices);
        return searchPersonResponse;
    }

    private void checkSearchPersonRequestValid(Request.Content.SearchPersonRequest searchPersonRequest) throws InvalidParameterException {
        String firstName = searchPersonRequest.getSearchedPerson().getFirstName();
        if (!isAlphabetical(firstName)) {
            throw new InvalidParameterException("Le prénom n'est pas bien formé");
        }
        String officialName = searchPersonRequest.getSearchedPerson().getOfficialName();
        if (!isAlphabetical(officialName)) {
            throw new InvalidParameterException("Le nom officiel n'est pas bien formé");
        }
        if (!isDateValid(searchPersonRequest.getSearchedPerson().getDateOfBirth())) {
            throw new InvalidParameterException("La date de naissance n'est pas bien formée");
        }
    }

    private boolean isAlphabetical(String param) {
        Pattern p = Pattern.compile("[-' \\p{IsLatin}]*");
        Matcher m = p.matcher(param);
        return m.matches();
    }

    private String getVnFromRow(Row row) {
        return getValueOfCell(row.getCell(0));
    }

    private Set<Row> getMatchingRows(Request.Content.SearchPersonRequest searchPersonRequest) throws DatatypeConfigurationException {
        Set<Row> rows = new HashSet<>();
        if (searchPersonRequest != null && searchPersonRequest.getSearchedPerson() != null) {
            rows.addAll(matchStringParameterInColumn(searchPersonRequest.getSearchedPerson().getFirstName(), COLUMN_FIRSTNAME));
            rows.addAll(matchStringParameterInColumn(searchPersonRequest.getSearchedPerson().getOfficialName(), COLUMN_OFFICIAL_NAME));

            if (searchPersonRequest.getSearchedPerson().getDateOfBirth() != null) {
                DatePartiallyKnownType dateOfBirth = searchPersonRequest.getSearchedPerson().getDateOfBirth();
                rows.addAll(searchDateOfBirthInSheet(dateOfBirth, SHEET_PERSONS));
            }
        }
        return selectBestMatchingRows(searchPersonRequest, rows);
    }

    private Set<Row> selectBestMatchingRows(Request.Content.SearchPersonRequest searchPersonRequest, Set<Row> rows) {
        int maximumNumberOfMatchingParam = 3;
        Set<Row> finalRows = new HashSet<>();
        for (Row row : rows) {
            int matchingParam = 0;
            if (StringUtils.containsIgnoreCase(getValueOfCell(row.getCell(7)), searchPersonRequest.getSearchedPerson().getFirstName())) {
                matchingParam += 1;
            }
            if (StringUtils.containsIgnoreCase(getValueOfCell(row.getCell(8)), searchPersonRequest.getSearchedPerson().getOfficialName())) {
                matchingParam += 1;
            }
            try {
                DatePartiallyKnownType dateOfBirth = new EchTransformer().getDateOfBirthFromRow(row);
                if (areEqualsDateOfBirth(dateOfBirth, searchPersonRequest.getSearchedPerson().getDateOfBirth())) {
                    matchingParam += 1;
                }
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
            }
            if (matchingParam == maximumNumberOfMatchingParam) {
                finalRows.add(row);
            } else if (matchingParam > 0 && matchingParam == maximumNumberOfMatchingParam - 1) {
                finalRows.add(row);
            }
        }
        return finalRows;
    }

    private List<Row> searchDateOfBirthInSheet(DatePartiallyKnownType param, String sheetName) throws DatatypeConfigurationException {
        List<Row> rows = new ArrayList<>();
        XSSFSheet sheet = workbook.getSheet(sheetName);

        if (param != null && param.getYearMonthDay() != null) {
            for (Row row : sheet) {
                if (row.getLastCellNum() > 5) {
                    DatePartiallyKnownType dateOfBirth = new EchTransformer().getDateOfBirthFromRow(row);
                    if (areEqualsDateOfBirth(param, dateOfBirth)) {
                        rows.add(row);
                    }
                }
            }
        }
        return rows;
    }

    private List<Row> matchStringParameterInColumn(String parameter, String columnName) {
        List<Row> rows = new ArrayList<>();
        if (StringUtils.isNotBlank(parameter)) {
            return searchParamInSheet(parameter, columnName, SHEET_PERSONS);
        }
        return rows;
    }


    private Response.PositiveResponse.CompareDataResponse initializeCompareDataResponse(Request.Content.CompareDataRequest compareDataRequest) {
        Response.PositiveResponse.CompareDataResponse compareDataResponse = new Response.PositiveResponse.CompareDataResponse();
        compareDataResponse.setCompareDataRequestId(compareDataRequest.getCompareDataRequestId());
        Response.PositiveResponse.CompareDataResponse.EchoPidsRequest echoPidsRequest = new Response.PositiveResponse.CompareDataResponse.EchoPidsRequest();
        echoPidsRequest.setVn(compareDataRequest.getPids().getVn());
        echoPidsRequest.setSPID(compareDataRequest.getPids().getSPID());
        compareDataResponse.setEchoPidsRequest(echoPidsRequest);
        return compareDataResponse;
    }

}
