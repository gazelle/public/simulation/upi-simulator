package net.ihe.gazelle.upi.exception;

public class MultipleFoundException extends Exception {
    public MultipleFoundException() {
        super();
    }

    public MultipleFoundException(String message) {
        super(message);
    }

    public MultipleFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public MultipleFoundException(Throwable cause) {
        super(cause);
    }

    protected MultipleFoundException(String message, Throwable cause,
                                     boolean enableSuppression,
                                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
