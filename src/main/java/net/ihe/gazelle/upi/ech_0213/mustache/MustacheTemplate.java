package net.ihe.gazelle.upi.ech_0213.mustache;

import com.samskivert.mustache.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class MustacheTemplate {

    Template template;

    public MustacheTemplate(String pathToTemplate) {
        Mustache.Compiler compiler = Mustache.compiler()
                .withLoader(name -> {
                    try {
                        return reader(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                    finally {
                        reader(name).close();
                    }
                });
        template = compiler.compile(reader(pathToTemplate));

    }

    public String execute(Object context) throws MustacheException {
        return template.execute(context);
    }

    private Reader reader(String name) {
        return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(name + ".mustache"), StandardCharsets.UTF_8));
    }

}
