package net.ihe.gazelle.upi.ech_0213.business;

import net.ihe.gazelle.upi.ech_0213.utils.ActionOnSPID;
import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.format.maps.MapCancel;
import net.ihe.gazelle.upi.format.maps.MapInactivate;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.format.maps.IMapToMustache;
import net.ihe.gazelle.upi.format.maps.MapGenerate;

import java.util.Map;


public class UPISedexManagerDispatcher {

    private ActionOnSPID action;
    private IMapToMustache iMapToMustache;

    public Map<String, String> actionOnSPIDToMap(Request req) throws NotFoundException {
        isRequestOrContentNull(req);
        String actionFromRequest = req.getContent().getActionOnSPID().toUpperCase();
        ActionOnSPID actionOnSpid = checkIfActionFromReqInEnum(actionFromRequest);

        if (actionOnSpid != null) {
            switch (actionOnSpid) {
                case GENERATE:
                    iMapToMustache = new MapGenerate();
                    break;
                case INACTIVATE:
                    iMapToMustache = new MapInactivate();
                    break;
                case CANCEL:
                    iMapToMustache = new MapCancel();
                    break;
            }
            return iMapToMustache.mapReqToMustache(req);
        } else {
            return getMapForNegativeReportInvalidActionOnSPID();
        }
    }

    private void isRequestOrContentNull(Request req) {
        if (req == null) {
            throw new NullPointerException("eCH-0213 Request from SOAP is null");
        } else if (req.getContent() == null) {
            throw new NullPointerException("ech-0123 Request.Content from SOAP is null");
        }
    }

    private ActionOnSPID checkIfActionFromReqInEnum(String actionFromRequest) {
        return action.fromString(actionFromRequest);
    }

    private Map<String, String> getMapForNegativeReportInvalidActionOnSPID() {
        IMapToMustache imptm = new MapGenerate();
        return imptm.mapForNegativeReport(WarnErrorKeyValue.CODE_ERROR_ACTION_ON_SPID_VALUE, WarnErrorKeyValue.MGG_ERROR_ACTION_ON_SPID_VALUE);
    }
}
