package net.ihe.gazelle.upi.ech_0213.business;

import net.ihe.gazelle.upi.ech_0213.mustache.MustacheTemplate;
import net.ihe.gazelle.upi.ech_0213.utils.UPITransformer;
import net.ihe.gazelle.upi.model.ConstantValues;
import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.model.ech_0213.Response;


import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.*;


public class ResponseManager {

    public Response createResponse(Request req) {
        UPISedexManagerDispatcher usmd = new UPISedexManagerDispatcher();
        try {
            Map<String, String> mapToMustache = usmd.actionOnSPIDToMap(req);
            if(mapToMustache.containsKey(WarnErrorKeyValue.NEGATIVE_REPORT)){
                return generateNegativeReport(mapToMustache);
            } else return getResponseFromMustache(mapToMustache);
        } catch (IOException | NotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Response getResponseFromMustache(Map<String, String> map) throws IOException {
        MustacheTemplate mt = new MustacheTemplate("/mustacheTemplates/ech_0213_" + map.get(ConstantValues.ACTION_ON_SPID) + "_response");
        String responseFromTemplate = mt.execute(map);

        try(ByteArrayInputStream responseContent = new ByteArrayInputStream(responseFromTemplate.getBytes())) {
            return UPITransformer.unmarshallMessage(Response.class, responseContent);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Response generateNegativeReport(Map<String, String> map) throws IOException {
        MustacheTemplate mt = new MustacheTemplate("/mustacheTemplates/ech_0213_negative_response");
        String responseFromTemplate = mt.execute(map);
        try (ByteArrayInputStream responseContent = new ByteArrayInputStream(responseFromTemplate.getBytes());) {
            return UPITransformer.unmarshallMessage(Response.class, responseContent);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}

