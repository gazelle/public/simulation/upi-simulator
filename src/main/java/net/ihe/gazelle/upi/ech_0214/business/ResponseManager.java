package net.ihe.gazelle.upi.ech_0214.business;

import net.ihe.gazelle.upi.adapter.Patient;
import net.ihe.gazelle.upi.adapter.PatientDAO;
import net.ihe.gazelle.upi.exception.InvalidParameterException;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.exception.UpiException;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import net.ihe.gazelle.upi.model.ech_0213_commons.NegativeReportType;
import net.ihe.gazelle.upi.model.ech_0214.Response.PositiveResponse.GetInfoPersonResponse;
import net.ihe.gazelle.upi.model.ech_0214.Request.Content.GetInfoPersonRequest;
import net.ihe.gazelle.upi.model.ech_0213_commons.NegativeReportType.Notice;
import net.ihe.gazelle.upi.ech_0214.mustache.Templating;

import java.util.ArrayList;
import java.util.List;

public class ResponseManager {
    public Response createResponse(Request request) {
        Response response = createResponseFromRequest(request);
        response.setHeader(request.getHeader());
        Templating templating = new Templating();
        try {
            response = templating.createResponse(response);
        } catch (Exception e) {
            response = initializeResponseWithNegativeReport(request, e.getMessage());
        }
        return response;
    }

    private void analyseRequest(Request request) throws InvalidParameterException {
        if (request.getContent() == null) {
            throw new InvalidParameterException("Le contenu de la requête est null");
        }
        if (request.getContent().getSPIDCategory() == null) {
            throw new InvalidParameterException("Le SPIDCategory de la requête est null");
        }
        if (request.getContent().getResponseLanguage() == null) {
            throw new InvalidParameterException("La langue attendue dans la réponse est null");
        }

    }

    private Response createResponseFromRequest(Request request) {
        Response response = new Response();
        Response.PositiveResponse positiveResponse = new Response.PositiveResponse();

        try {
            analyseRequest(request);

            String spidCategory = request.getContent().getSPIDCategory();
            checkRequestTypes(request);
            PatientDAO patient = new Patient();

            List<GetInfoPersonResponse> getInfoPersonResponses = createGetInfoPersonResponsesFromRequest(request, patient);
            List<Response.PositiveResponse.SearchPersonResponse> searchPersonResponses = createSearchPersonResponsesFromRequest(request, patient);
            List<Response.PositiveResponse.CompareDataResponse> compareDataResponses = createCompareDataResponsesFromRequest(request, patient);

            positiveResponse.setGetInfoPersonResponse(getInfoPersonResponses);
            positiveResponse.setSearchPersonResponse(searchPersonResponses);
            positiveResponse.setCompareDataResponse(compareDataResponses);
            positiveResponse.setSPIDCategory(spidCategory);

            response.setPositiveResponse(positiveResponse);

            checkResponseTypes(getInfoPersonResponses, searchPersonResponses, compareDataResponses);

        } catch (InvalidParameterException | UpiException | NotFoundException e) {
            response = initializeResponseWithNegativeReport(request, e.getMessage());
        }
        return response;
    }


    private List<GetInfoPersonResponse> createGetInfoPersonResponsesFromRequest(Request request, PatientDAO patient) {
        List<GetInfoPersonResponse> getInfoPersonResponses = new ArrayList<>();
        if (request.getContent().getGetInfoPersonRequest() != null) {
            for (GetInfoPersonRequest getInfoPersonRequest : request.getContent().getGetInfoPersonRequest()) {
                getInfoPersonResponses.add(createGetInfoPersonResponseFromRequest(getInfoPersonRequest, patient));
            }
        }
        return getInfoPersonResponses;
    }

    private GetInfoPersonResponse createGetInfoPersonResponseFromRequest(GetInfoPersonRequest getInfoPersonRequest, PatientDAO patient) {
        Response.PositiveResponse.GetInfoPersonResponse getInfoPersonResponse;
        try {
            getInfoPersonResponse = patient.getInfoPersonFromPersonRequest(getInfoPersonRequest);
            getInfoPersonResponse.setGetInfoPersonRequestId(getInfoPersonRequest.getGetInfoPersonRequestId());
            Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest echoPidRequest = new Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest();
            if (getInfoPersonRequest.getPid() != null && getInfoPersonRequest.getPid().getSPID() != null) {
                echoPidRequest.setSPID(getInfoPersonRequest.getPid().getSPID());
            }
            if (getInfoPersonRequest.getPid() != null && getInfoPersonRequest.getPid().getVn() != null) {
                echoPidRequest.setVn(getInfoPersonRequest.getPid().getVn());
            }
            getInfoPersonResponse.setEchoPidRequest(echoPidRequest);

        } catch (NotFoundException | InvalidParameterException | MultipleFoundException e) {
            getInfoPersonResponse = new Response.PositiveResponse.GetInfoPersonResponse();
            getInfoPersonResponse.setGetInfoPersonRequestId(getInfoPersonRequest.getGetInfoPersonRequestId());
            getInfoPersonResponse.setNegativReportOnGetInfoPerson(generateNegativeReportType(e.getMessage()));
        }
        return getInfoPersonResponse;
    }

    private List<Response.PositiveResponse.SearchPersonResponse> createSearchPersonResponsesFromRequest(Request request, PatientDAO patient) {
        List<Response.PositiveResponse.SearchPersonResponse> searchPersonResponses = new ArrayList<>();
        if (request.getContent().getSearchPersonRequest() != null) {
            for (Request.Content.SearchPersonRequest searchPersonRequest : request.getContent().getSearchPersonRequest()) {
                searchPersonResponses.add(createSearchPersonResponseFromRequest(searchPersonRequest, patient));
            }
        }
        return searchPersonResponses;
    }

    private Response.PositiveResponse.SearchPersonResponse createSearchPersonResponseFromRequest(Request.Content.SearchPersonRequest searchPersonRequest, PatientDAO patient) {
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse;
        try {
            searchPersonResponse = patient.getSearchPersonResponseFromSearchPersonRequest(searchPersonRequest);

        } catch (NotFoundException | InvalidParameterException e) {
            searchPersonResponse = new Response.PositiveResponse.SearchPersonResponse();
            searchPersonResponse.setNegativReportOnSearchPerson(generateNegativeReportType(e.getMessage()));
        }
        searchPersonResponse.setSearchPersonRequestId(searchPersonRequest.getSearchPersonRequestId());
        return searchPersonResponse;
    }

    private List<Response.PositiveResponse.CompareDataResponse> createCompareDataResponsesFromRequest(Request request, PatientDAO patient) {
        List<Response.PositiveResponse.CompareDataResponse> compareDataResponses = new ArrayList<>();
        if (request.getContent().getCompareDataRequest() != null) {
            for (Request.Content.CompareDataRequest compareDataRequest : request.getContent().getCompareDataRequest()) {
                compareDataResponses.add(createCompareDataResponseFromRequest(compareDataRequest, patient));
            }
        }
        return compareDataResponses;
    }

    private Response.PositiveResponse.CompareDataResponse createCompareDataResponseFromRequest(Request.Content.CompareDataRequest compareDataRequest, PatientDAO patient) {
        Response.PositiveResponse.CompareDataResponse compareDataResponse;
        try {
            compareDataResponse = patient.getCompareDataResponseFromDataRequest(compareDataRequest);
            compareDataResponse.setCompareDataRequestId(compareDataRequest.getCompareDataRequestId());
        } catch (NotFoundException | MultipleFoundException e) {
            compareDataResponse = new Response.PositiveResponse.CompareDataResponse();
            compareDataResponse.setCompareDataRequestId(compareDataRequest.getCompareDataRequestId());
            compareDataResponse.setNegativReportOnCompareData(generateNegativeReportType(e.getMessage()));
        }
        return compareDataResponse;
    }


    private void checkResponseTypes(List<Response.PositiveResponse.GetInfoPersonResponse> getInfoPersonResponses,
                                    List<Response.PositiveResponse.SearchPersonResponse> searchPersonResponses,
                                    List<Response.PositiveResponse.CompareDataResponse> compareDataResponses) throws UpiException {
        if (getInfoPersonResponses.isEmpty() && searchPersonResponses.isEmpty() && compareDataResponses.isEmpty()) {
            throw new UpiException("Aucune réponse n'a pu être fournie");
        }
    }

    private void checkRequestTypes(Request request) throws InvalidParameterException {
        int sizeInfoPersonRequest = 0;
        int sizeSearchPersonRequest = 0;
        int sizeCompareDataRequest = 0;

        if (request.getContent().getGetInfoPersonRequest() != null) {
            sizeInfoPersonRequest = request.getContent().getGetInfoPersonRequest().size();
        }
        if (request.getContent().getSearchPersonRequest() != null) {
            sizeSearchPersonRequest = request.getContent().getSearchPersonRequest().size();
        }
        if (request.getContent().getCompareDataRequest() != null) {
            sizeCompareDataRequest = request.getContent().getCompareDataRequest().size();
        }
        if ((sizeInfoPersonRequest != 0 && sizeSearchPersonRequest != 0) ||
                (sizeInfoPersonRequest != 0 && sizeCompareDataRequest != 0) || (
                sizeSearchPersonRequest != 0 && sizeCompareDataRequest != 0)) {
            throw new InvalidParameterException("Plusieurs requêtes ayant des types différents détectées");
        }
    }

    private Response initializeResponseWithNegativeReport(Request request, String errorMessage) {
        Response response = new Response();
        response.setHeader(request.getHeader());
        response.setPositiveResponse(null);
        response.setNegativeReport(generateNegativeReportType(errorMessage));
        return response;
    }

    private NegativeReportType generateNegativeReportType(String errorMessage) {
        NegativeReportType negativeReportType = new NegativeReportType();
        Notice notice = new NegativeReportType.Notice();
        notice.setCodeDescription(errorMessage);
        negativeReportType.setNotice(notice);
        return negativeReportType;
    }
}
