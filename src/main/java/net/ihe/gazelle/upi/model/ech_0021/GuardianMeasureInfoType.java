
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour guardianMeasureInfoType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="guardianMeasureInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="basedOnLaw" type="{http://www.ech.ch/xmlns/eCH-0021/7}basedOnLawType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="basedOnLawAddOn" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="guardianMeasureValidFrom" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "guardianMeasureInfoType", propOrder = {
    "basedOnLaw",
    "basedOnLawAddOn",
    "guardianMeasureValidFrom"
})
public class GuardianMeasureInfoType {

    protected List<String> basedOnLaw;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String basedOnLawAddOn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar guardianMeasureValidFrom;

    /**
     * Gets the value of the basedOnLaw property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the basedOnLaw property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBasedOnLaw().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBasedOnLaw() {
        if (basedOnLaw == null) {
            basedOnLaw = new ArrayList<String>();
        }
        return this.basedOnLaw;
    }

    /**
     * Obtient la valeur de la propriété basedOnLawAddOn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasedOnLawAddOn() {
        return basedOnLawAddOn;
    }

    /**
     * Définit la valeur de la propriété basedOnLawAddOn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasedOnLawAddOn(String value) {
        this.basedOnLawAddOn = value;
    }

    /**
     * Obtient la valeur de la propriété guardianMeasureValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGuardianMeasureValidFrom() {
        return guardianMeasureValidFrom;
    }

    /**
     * Définit la valeur de la propriété guardianMeasureValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGuardianMeasureValidFrom(XMLGregorianCalendar value) {
        this.guardianMeasureValidFrom = value;
    }

}
