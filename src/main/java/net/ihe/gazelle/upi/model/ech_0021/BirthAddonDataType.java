
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour birthAddonDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="birthAddonDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nameOfFather" type="{http://www.ech.ch/xmlns/eCH-0021/7}nameOfParentType" minOccurs="0"/&gt;
 *         &lt;element name="nameOfMother" type="{http://www.ech.ch/xmlns/eCH-0021/7}nameOfParentType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "birthAddonDataType", propOrder = {
    "nameOfFather",
    "nameOfMother"
})
public class BirthAddonDataType {

    protected NameOfParentType nameOfFather;
    protected NameOfParentType nameOfMother;

    /**
     * Obtient la valeur de la propriété nameOfFather.
     * 
     * @return
     *     possible object is
     *     {@link NameOfParentType }
     *     
     */
    public NameOfParentType getNameOfFather() {
        return nameOfFather;
    }

    /**
     * Définit la valeur de la propriété nameOfFather.
     * 
     * @param value
     *     allowed object is
     *     {@link NameOfParentType }
     *     
     */
    public void setNameOfFather(NameOfParentType value) {
        this.nameOfFather = value;
    }

    /**
     * Obtient la valeur de la propriété nameOfMother.
     * 
     * @return
     *     possible object is
     *     {@link NameOfParentType }
     *     
     */
    public NameOfParentType getNameOfMother() {
        return nameOfMother;
    }

    /**
     * Définit la valeur de la propriété nameOfMother.
     * 
     * @param value
     *     allowed object is
     *     {@link NameOfParentType }
     *     
     */
    public void setNameOfMother(NameOfParentType value) {
        this.nameOfMother = value;
    }

}
