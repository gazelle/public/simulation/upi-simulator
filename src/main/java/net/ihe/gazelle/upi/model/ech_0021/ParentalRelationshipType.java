
package net.ihe.gazelle.upi.model.ech_0021;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.ihe.gazelle.upi.model.ech_0010.PersonMailAddressType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationLightType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationType;


/**
 * <p>Classe Java pour parentalRelationshipType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="parentalRelationshipType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partner"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
 *                     &lt;element name="personIdentificationPartner" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationLightType"/&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element name="address" type="{http://www.ech.ch/xmlns/eCH-0010/5}personMailAddressType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="relationshipValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="typeOfRelationship"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.ech.ch/xmlns/eCH-0021/7}typeOfRelationshipType"&gt;
 *               &lt;enumeration value="3"/&gt;
 *               &lt;enumeration value="4"/&gt;
 *               &lt;enumeration value="5"/&gt;
 *               &lt;enumeration value="6"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="care" type="{http://www.ech.ch/xmlns/eCH-0021/7}careType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parentalRelationshipType", propOrder = {
    "partner",
    "relationshipValidFrom",
    "typeOfRelationship",
    "care"
})
public class ParentalRelationshipType {

    @XmlElement(required = true)
    protected ParentalRelationshipType.Partner partner;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar relationshipValidFrom;
    @XmlElement(required = true)
    protected String typeOfRelationship;
    @XmlElement(required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger care;

    /**
     * Obtient la valeur de la propriété partner.
     * 
     * @return
     *     possible object is
     *     {@link ParentalRelationshipType.Partner }
     *     
     */
    public ParentalRelationshipType.Partner getPartner() {
        return partner;
    }

    /**
     * Définit la valeur de la propriété partner.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentalRelationshipType.Partner }
     *     
     */
    public void setPartner(ParentalRelationshipType.Partner value) {
        this.partner = value;
    }

    /**
     * Obtient la valeur de la propriété relationshipValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRelationshipValidFrom() {
        return relationshipValidFrom;
    }

    /**
     * Définit la valeur de la propriété relationshipValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRelationshipValidFrom(XMLGregorianCalendar value) {
        this.relationshipValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété typeOfRelationship.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOfRelationship() {
        return typeOfRelationship;
    }

    /**
     * Définit la valeur de la propriété typeOfRelationship.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOfRelationship(String value) {
        this.typeOfRelationship = value;
    }

    /**
     * Obtient la valeur de la propriété care.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCare() {
        return care;
    }

    /**
     * Définit la valeur de la propriété care.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCare(BigInteger value) {
        this.care = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
     *           &lt;element name="personIdentificationPartner" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationLightType"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element name="address" type="{http://www.ech.ch/xmlns/eCH-0010/5}personMailAddressType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personIdentification",
        "personIdentificationPartner",
        "address"
    })
    public static class Partner {

        protected PersonIdentificationType personIdentification;
        protected PersonIdentificationLightType personIdentificationPartner;
        protected PersonMailAddressType address;

        /**
         * Obtient la valeur de la propriété personIdentification.
         * 
         * @return
         *     possible object is
         *     {@link PersonIdentificationType }
         *     
         */
        public PersonIdentificationType getPersonIdentification() {
            return personIdentification;
        }

        /**
         * Définit la valeur de la propriété personIdentification.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonIdentificationType }
         *     
         */
        public void setPersonIdentification(PersonIdentificationType value) {
            this.personIdentification = value;
        }

        /**
         * Obtient la valeur de la propriété personIdentificationPartner.
         * 
         * @return
         *     possible object is
         *     {@link PersonIdentificationLightType }
         *     
         */
        public PersonIdentificationLightType getPersonIdentificationPartner() {
            return personIdentificationPartner;
        }

        /**
         * Définit la valeur de la propriété personIdentificationPartner.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonIdentificationLightType }
         *     
         */
        public void setPersonIdentificationPartner(PersonIdentificationLightType value) {
            this.personIdentificationPartner = value;
        }

        /**
         * Obtient la valeur de la propriété address.
         * 
         * @return
         *     possible object is
         *     {@link PersonMailAddressType }
         *     
         */
        public PersonMailAddressType getAddress() {
            return address;
        }

        /**
         * Définit la valeur de la propriété address.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonMailAddressType }
         *     
         */
        public void setAddress(PersonMailAddressType value) {
            this.address = value;
        }

    }

}
