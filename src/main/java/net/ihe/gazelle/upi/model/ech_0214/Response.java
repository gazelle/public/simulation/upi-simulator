
package net.ihe.gazelle.upi.model.ech_0214;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.upi.model.ech_0058.HeaderType;
import net.ihe.gazelle.upi.model.ech_0213_commons.NegativeReportType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsFromUPIType;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.ech.ch/xmlns/eCH-0058/5}headerType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="positiveResponse"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
 *                     &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                       &lt;element name="globalNotice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
 *                     &lt;/sequence&gt;
 *                     &lt;choice&gt;
 *                       &lt;element name="getInfoPersonResponse" maxOccurs="unbounded"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                                 &lt;choice&gt;
 *                                   &lt;sequence&gt;
 *                                     &lt;element name="echoPidRequest"&gt;
 *                                       &lt;complexType&gt;
 *                                         &lt;complexContent&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                             &lt;choice&gt;
 *                                               &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
 *                                               &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
 *                                             &lt;/choice&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/complexContent&gt;
 *                                       &lt;/complexType&gt;
 *                                     &lt;/element&gt;
 *                                     &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                                       &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
 *                                       &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/sequence&gt;
 *                                   &lt;element name="negativReportOnGetInfoPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
 *                                 &lt;/choice&gt;
 *                               &lt;/sequence&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                       &lt;element name="searchPersonResponse" maxOccurs="unbounded"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                                 &lt;choice&gt;
 *                                   &lt;sequence&gt;
 *                                     &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
 *                                     &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                                       &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;choice&gt;
 *                                       &lt;element name="found"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
 *                                                 &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="maybeFound"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="candidate" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
 *                                                           &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
 *                                                           &lt;element name="historicalValuesPersonFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                                                           &lt;element name="candidateLikeliness" type="{http://www.ech.ch/xmlns/eCH-0214/2}candidateLikelinessType" minOccurs="0"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="notFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                                     &lt;/choice&gt;
 *                                   &lt;/sequence&gt;
 *                                   &lt;element name="negativReportOnSearchPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
 *                                 &lt;/choice&gt;
 *                               &lt;/sequence&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                       &lt;element name="compareDataResponse" maxOccurs="unbounded"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                                 &lt;choice&gt;
 *                                   &lt;sequence&gt;
 *                                     &lt;element name="echoPidsRequest"&gt;
 *                                       &lt;complexType&gt;
 *                                         &lt;complexContent&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                             &lt;sequence&gt;
 *                                               &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
 *                                               &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
 *                                             &lt;/sequence&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/complexContent&gt;
 *                                       &lt;/complexType&gt;
 *                                     &lt;/element&gt;
 *                                     &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                                       &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;choice&gt;
 *                                       &lt;element name="identicalData" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                                       &lt;element name="differentData"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/choice&gt;
 *                                   &lt;/sequence&gt;
 *                                   &lt;element name="negativReportOnCompareData" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
 *                                 &lt;/choice&gt;
 *                               &lt;/sequence&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                     &lt;/choice&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="negativeReportType" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="minorVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "positiveResponse",
        "negativeReport"
})
@XmlRootElement(name = "response")
public class Response {

    @XmlElement(required = true)
    protected HeaderType header;
    @XmlElement
    protected Response.PositiveResponse positiveResponse;
    @XmlElement
    protected NegativeReportType negativeReport;
    @XmlAttribute(name = "minorVersion", required = true)
    protected BigInteger minorVersion;

    /**
     * Obtient la valeur de la propriété header.
     *
     * @return possible object is
     * {@link HeaderType }
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Définit la valeur de la propriété header.
     *
     * @param value allowed object is
     *              {@link HeaderType }
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propriété positiveResponse.
     *
     * @return possible object is
     * {@link Response.PositiveResponse }
     */
    public Response.PositiveResponse getPositiveResponse() {
        return positiveResponse;
    }

    /**
     * Définit la valeur de la propriété positiveResponse.
     *
     * @param value allowed object is
     *              {@link Response.PositiveResponse }
     */
    public void setPositiveResponse(Response.PositiveResponse value) {
        this.positiveResponse = value;
    }

    /**
     * Obtient la valeur de la propriété negativeReportType.
     *
     * @return possible object is
     * {@link NegativeReportType }
     */
    public NegativeReportType getNegativeReport() {
        return negativeReport;
    }
//    public NegativeReportType getNegativeReport() {
//        return negativeReport;
//    }

    /**
     * Définit la valeur de la propriété negativeReportType.
     *
     * @param value allowed object is
     *              {@link NegativeReportType }
     */
    public void setNegativeReport(NegativeReportType value) {
        this.negativeReport = value;
    }
//    public void setNegativeReport(NegativeReportType value) {
//        this.negativeReport = value;
//    }

    /**
     * Obtient la valeur de la propriété minorVersion.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getMinorVersion() {
        return minorVersion;
    }

    /**
     * Définit la valeur de la propriété minorVersion.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setMinorVersion(BigInteger value) {
        this.minorVersion = value;
    }
    public String toString() {
        return new ReflectionToStringBuilder(this, new RecursiveToStringStyle()).toString();
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
     *         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;element name="globalNotice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="getInfoPersonResponse" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;choice&gt;
     *                       &lt;sequence&gt;
     *                         &lt;element name="echoPidRequest"&gt;
     *                           &lt;complexType&gt;
     *                             &lt;complexContent&gt;
     *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                 &lt;choice&gt;
     *                                   &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
     *                                   &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
     *                                 &lt;/choice&gt;
     *                               &lt;/restriction&gt;
     *                             &lt;/complexContent&gt;
     *                           &lt;/complexType&gt;
     *                         &lt;/element&gt;
     *                         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                           &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
     *                         &lt;/sequence&gt;
     *                         &lt;sequence&gt;
     *                           &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
     *                           &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" minOccurs="0"/&gt;
     *                         &lt;/sequence&gt;
     *                       &lt;/sequence&gt;
     *                       &lt;element name="negativReportOnGetInfoPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
     *                     &lt;/choice&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *           &lt;element name="searchPersonResponse" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;choice&gt;
     *                       &lt;sequence&gt;
     *                         &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
     *                         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                           &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
     *                         &lt;/sequence&gt;
     *                         &lt;choice&gt;
     *                           &lt;element name="found"&gt;
     *                             &lt;complexType&gt;
     *                               &lt;complexContent&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                   &lt;sequence&gt;
     *                                     &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
     *                                     &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
     *                                   &lt;/sequence&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/complexContent&gt;
     *                             &lt;/complexType&gt;
     *                           &lt;/element&gt;
     *                           &lt;element name="maybeFound"&gt;
     *                             &lt;complexType&gt;
     *                               &lt;complexContent&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                   &lt;sequence&gt;
     *                                     &lt;element name="candidate" maxOccurs="unbounded"&gt;
     *                                       &lt;complexType&gt;
     *                                         &lt;complexContent&gt;
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                             &lt;sequence&gt;
     *                                               &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
     *                                               &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
     *                                               &lt;element name="historicalValuesPersonFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                                               &lt;element name="candidateLikeliness" type="{http://www.ech.ch/xmlns/eCH-0214/2}candidateLikelinessType" minOccurs="0"/&gt;
     *                                             &lt;/sequence&gt;
     *                                           &lt;/restriction&gt;
     *                                         &lt;/complexContent&gt;
     *                                       &lt;/complexType&gt;
     *                                     &lt;/element&gt;
     *                                   &lt;/sequence&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/complexContent&gt;
     *                             &lt;/complexType&gt;
     *                           &lt;/element&gt;
     *                           &lt;element name="notFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                         &lt;/choice&gt;
     *                       &lt;/sequence&gt;
     *                       &lt;element name="negativReportOnSearchPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
     *                     &lt;/choice&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *           &lt;element name="compareDataResponse" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;choice&gt;
     *                       &lt;sequence&gt;
     *                         &lt;element name="echoPidsRequest"&gt;
     *                           &lt;complexType&gt;
     *                             &lt;complexContent&gt;
     *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                 &lt;sequence&gt;
     *                                   &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
     *                                   &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
     *                                 &lt;/sequence&gt;
     *                               &lt;/restriction&gt;
     *                             &lt;/complexContent&gt;
     *                           &lt;/complexType&gt;
     *                         &lt;/element&gt;
     *                         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                           &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
     *                         &lt;/sequence&gt;
     *                         &lt;choice&gt;
     *                           &lt;element name="identicalData" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                           &lt;element name="differentData"&gt;
     *                             &lt;complexType&gt;
     *                               &lt;complexContent&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                   &lt;sequence&gt;
     *                                     &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
     *                                   &lt;/sequence&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/complexContent&gt;
     *                             &lt;/complexType&gt;
     *                           &lt;/element&gt;
     *                         &lt;/choice&gt;
     *                       &lt;/sequence&gt;
     *                       &lt;element name="negativReportOnCompareData" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
     *                     &lt;/choice&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "spidCategory",
            "globalNotice",
            "getInfoPersonResponse",
            "searchPersonResponse",
            "compareDataResponse"
    })
    public static class PositiveResponse {

        @XmlElement(name = "SPIDCategory", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String spidCategory;
        protected List<Notice> globalNotice;
        protected List<Response.PositiveResponse.GetInfoPersonResponse> getInfoPersonResponse;
        protected List<Response.PositiveResponse.SearchPersonResponse> searchPersonResponse;
        protected List<Response.PositiveResponse.CompareDataResponse> compareDataResponse;

        /**
         * Obtient la valeur de la propriété spidCategory.
         *
         * @return possible object is
         * {@link String }
         */
        public String getSPIDCategory() {
            return spidCategory;
        }

        /**
         * Définit la valeur de la propriété spidCategory.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setSPIDCategory(String value) {
            this.spidCategory = value;
        }

        /**
         * Gets the value of the globalNotice property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the globalNotice property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGlobalNotice().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Notice }
         */
        public List<Notice> getGlobalNotice() {
            if (globalNotice == null) {
                globalNotice = new ArrayList<Notice>();
            }
            return this.globalNotice;
        }

        /**
         * Gets the value of the getInfoPersonResponse property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the getInfoPersonResponse property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGetInfoPersonResponse().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Response.PositiveResponse.GetInfoPersonResponse }
         */
        public List<Response.PositiveResponse.GetInfoPersonResponse> getGetInfoPersonResponse() {
            if (getInfoPersonResponse == null) {
                getInfoPersonResponse = new ArrayList<Response.PositiveResponse.GetInfoPersonResponse>();
            }
            return this.getInfoPersonResponse;
        }

        public void setGetInfoPersonResponse(List<GetInfoPersonResponse> getInfoPersonResponse) {
            this.getInfoPersonResponse = getInfoPersonResponse;
        }

        /**
         * Gets the value of the searchPersonResponse property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the searchPersonResponse property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSearchPersonResponse().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Response.PositiveResponse.SearchPersonResponse }
         */
        public List<Response.PositiveResponse.SearchPersonResponse> getSearchPersonResponse() {
            if (searchPersonResponse == null) {
                searchPersonResponse = new ArrayList<Response.PositiveResponse.SearchPersonResponse>();
            }
            return this.searchPersonResponse;
        }

        public void setSearchPersonResponse(List<SearchPersonResponse> searchPersonResponse) {
            this.searchPersonResponse = searchPersonResponse;
        }

        /**
         * Gets the value of the compareDataResponse property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the compareDataResponse property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCompareDataResponse().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Response.PositiveResponse.CompareDataResponse }
         */
        public List<Response.PositiveResponse.CompareDataResponse> getCompareDataResponse() {
            if (compareDataResponse == null) {
                compareDataResponse = new ArrayList<Response.PositiveResponse.CompareDataResponse>();
            }
            return this.compareDataResponse;
        }

        public void setCompareDataResponse(List<CompareDataResponse> compareDataResponse) {
            this.compareDataResponse = compareDataResponse;
        }

        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;choice&gt;
         *           &lt;sequence&gt;
         *             &lt;element name="echoPidsRequest"&gt;
         *               &lt;complexType&gt;
         *                 &lt;complexContent&gt;
         *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                     &lt;sequence&gt;
         *                       &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
         *                       &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
         *                     &lt;/sequence&gt;
         *                   &lt;/restriction&gt;
         *                 &lt;/complexContent&gt;
         *               &lt;/complexType&gt;
         *             &lt;/element&gt;
         *             &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *               &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
         *             &lt;/sequence&gt;
         *             &lt;choice&gt;
         *               &lt;element name="identicalData" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *               &lt;element name="differentData"&gt;
         *                 &lt;complexType&gt;
         *                   &lt;complexContent&gt;
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                       &lt;sequence&gt;
         *                         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
         *                       &lt;/sequence&gt;
         *                     &lt;/restriction&gt;
         *                   &lt;/complexContent&gt;
         *                 &lt;/complexType&gt;
         *               &lt;/element&gt;
         *             &lt;/choice&gt;
         *           &lt;/sequence&gt;
         *           &lt;element name="negativReportOnCompareData" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
         *         &lt;/choice&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "compareDataRequestId",
                "echoPidsRequest",
                "notice",
                "identicalData",
                "differentData",
                "negativReportOnCompareData"
        })
        public static class CompareDataResponse {

            @XmlElement(required = true)
            protected BigInteger compareDataRequestId;
            protected Response.PositiveResponse.CompareDataResponse.EchoPidsRequest echoPidsRequest;
            protected List<Notice> notice;
            protected Object identicalData;
            protected Response.PositiveResponse.CompareDataResponse.DifferentData differentData;
            protected NegativeReportType negativReportOnCompareData;

            /**
             * Obtient la valeur de la propriété compareDataRequestId.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getCompareDataRequestId() {
                return compareDataRequestId;
            }

            /**
             * Définit la valeur de la propriété compareDataRequestId.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setCompareDataRequestId(BigInteger value) {
                this.compareDataRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété echoPidsRequest.
             *
             * @return possible object is
             * {@link Response.PositiveResponse.CompareDataResponse.EchoPidsRequest }
             */
            public Response.PositiveResponse.CompareDataResponse.EchoPidsRequest getEchoPidsRequest() {
                return echoPidsRequest;
            }

            /**
             * Définit la valeur de la propriété echoPidsRequest.
             *
             * @param value allowed object is
             *              {@link Response.PositiveResponse.CompareDataResponse.EchoPidsRequest }
             */
            public void setEchoPidsRequest(Response.PositiveResponse.CompareDataResponse.EchoPidsRequest value) {
                this.echoPidsRequest = value;
            }

            /**
             * Gets the value of the notice property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the notice property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNotice().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Notice }
             */
            public List<Notice> getNotice() {
                if (notice == null) {
                    notice = new ArrayList<Notice>();
                }
                return this.notice;
            }

            /**
             * Obtient la valeur de la propriété identicalData.
             *
             * @return possible object is
             * {@link Object }
             */
            public Object getIdenticalData() {
                return identicalData;
            }

            /**
             * Définit la valeur de la propriété identicalData.
             *
             * @param value allowed object is
             *              {@link Object }
             */
            public void setIdenticalData(Object value) {
                this.identicalData = value;
            }

            /**
             * Obtient la valeur de la propriété differentData.
             *
             * @return possible object is
             * {@link Response.PositiveResponse.CompareDataResponse.DifferentData }
             */
            public Response.PositiveResponse.CompareDataResponse.DifferentData getDifferentData() {
                return differentData;
            }

            /**
             * Définit la valeur de la propriété differentData.
             *
             * @param value allowed object is
             *              {@link Response.PositiveResponse.CompareDataResponse.DifferentData }
             */
            public void setDifferentData(Response.PositiveResponse.CompareDataResponse.DifferentData value) {
                this.differentData = value;
            }

            /**
             * Obtient la valeur de la propriété negativReportOnCompareData.
             *
             * @return possible object is
             * {@link NegativeReportType }
             */
            public NegativeReportType getNegativReportOnCompareData() {
                return negativReportOnCompareData;
            }

            /**
             * Définit la valeur de la propriété negativReportOnCompareData.
             *
             * @param value allowed object is
             *              {@link NegativeReportType }
             */
            public void setNegativReportOnCompareData(NegativeReportType value) {
                this.negativReportOnCompareData = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "pids"
            })
            public static class DifferentData {

                @XmlElement(required = true)
                protected PidsFromUPIType pids;

                /**
                 * Obtient la valeur de la propriété pids.
                 *
                 * @return possible object is
                 * {@link PidsFromUPIType }
                 */
                public PidsFromUPIType getPids() {
                    return pids;
                }

                /**
                 * Définit la valeur de la propriété pids.
                 *
                 * @param value allowed object is
                 *              {@link PidsFromUPIType }
                 */
                public void setPids(PidsFromUPIType value) {
                    this.pids = value;
                }

            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
             *         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vn",
                    "spid"
            })
            public static class EchoPidsRequest {

                @XmlSchemaType(name = "unsignedLong")
                protected long vn;
                @XmlElement(name = "SPID", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "token")
                protected String spid;

                /**
                 * Obtient la valeur de la propriété vn.
                 */
                public long getVn() {
                    return vn;
                }

                /**
                 * Définit la valeur de la propriété vn.
                 */
                public void setVn(long value) {
                    this.vn = value;
                }

                /**
                 * Obtient la valeur de la propriété spid.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getSPID() {
                    return spid;
                }

                /**
                 * Définit la valeur de la propriété spid.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setSPID(String value) {
                    this.spid = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;choice&gt;
         *           &lt;sequence&gt;
         *             &lt;element name="echoPidRequest"&gt;
         *               &lt;complexType&gt;
         *                 &lt;complexContent&gt;
         *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                     &lt;choice&gt;
         *                       &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
         *                       &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
         *                     &lt;/choice&gt;
         *                   &lt;/restriction&gt;
         *                 &lt;/complexContent&gt;
         *               &lt;/complexType&gt;
         *             &lt;/element&gt;
         *             &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *               &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
         *             &lt;/sequence&gt;
         *             &lt;sequence&gt;
         *               &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
         *               &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" minOccurs="0"/&gt;
         *             &lt;/sequence&gt;
         *           &lt;/sequence&gt;
         *           &lt;element name="negativReportOnGetInfoPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
         *         &lt;/choice&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "getInfoPersonRequestId",
                "echoPidRequest",
                "notice",
                "pids",
                "personFromUPI",
                "negativReportOnGetInfoPerson"
        })
        public static class GetInfoPersonResponse {

            @XmlElement(required = true)
            protected BigInteger getInfoPersonRequestId;
            protected Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest echoPidRequest;
            protected List<Notice> notice;
            protected PidsFromUPIType pids;
            protected PersonFromUPIType personFromUPI;
            protected NegativeReportType negativReportOnGetInfoPerson;

            /**
             * Obtient la valeur de la propriété getInfoPersonRequestId.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getGetInfoPersonRequestId() {
                return getInfoPersonRequestId;
            }

            /**
             * Définit la valeur de la propriété getInfoPersonRequestId.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setGetInfoPersonRequestId(BigInteger value) {
                this.getInfoPersonRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété echoPidRequest.
             *
             * @return possible object is
             * {@link Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest }
             */
            public Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest getEchoPidRequest() {
                return echoPidRequest;
            }

            /**
             * Définit la valeur de la propriété echoPidRequest.
             *
             * @param value allowed object is
             *              {@link Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest }
             */
            public void setEchoPidRequest(Response.PositiveResponse.GetInfoPersonResponse.EchoPidRequest value) {
                this.echoPidRequest = value;
            }

            /**
             * Gets the value of the notice property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the notice property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNotice().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Notice }
             */
            public List<Notice> getNotice() {
                if (notice == null) {
                    notice = new ArrayList<Notice>();
                }
                return this.notice;
            }

            /**
             * Obtient la valeur de la propriété pids.
             *
             * @return possible object is
             * {@link PidsFromUPIType }
             */
            public PidsFromUPIType getPids() {
                return pids;
            }

            /**
             * Définit la valeur de la propriété pids.
             *
             * @param value allowed object is
             *              {@link PidsFromUPIType }
             */
            public void setPids(PidsFromUPIType value) {
                this.pids = value;
            }

            /**
             * Obtient la valeur de la propriété personFromUPI.
             *
             * @return possible object is
             * {@link PersonFromUPIType }
             */
            public PersonFromUPIType getPersonFromUPI() {
                return personFromUPI;
            }

            /**
             * Définit la valeur de la propriété personFromUPI.
             *
             * @param value allowed object is
             *              {@link PersonFromUPIType }
             */
            public void setPersonFromUPI(PersonFromUPIType value) {
                this.personFromUPI = value;
            }

            /**
             * Obtient la valeur de la propriété negativReportOnGetInfoPerson.
             *
             * @return possible object is
             * {@link NegativeReportType }
             */
            public NegativeReportType getNegativReportOnGetInfoPerson() {
                return negativReportOnGetInfoPerson;
            }

            /**
             * Définit la valeur de la propriété negativReportOnGetInfoPerson.
             *
             * @param value allowed object is
             *              {@link NegativeReportType }
             */
            public void setNegativReportOnGetInfoPerson(NegativeReportType value) {
                this.negativReportOnGetInfoPerson = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;choice&gt;
             *         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
             *         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
             *       &lt;/choice&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "vn",
                    "spid"
            })
            public static class EchoPidRequest {

                @XmlSchemaType(name = "unsignedLong")
                protected Long vn;
                @XmlElement(name = "SPID")
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "token")
                protected String spid;

                /**
                 * Obtient la valeur de la propriété vn.
                 *
                 * @return possible object is
                 * {@link Long }
                 */
                public Long getVn() {
                    return vn;
                }

                /**
                 * Définit la valeur de la propriété vn.
                 *
                 * @param value allowed object is
                 *              {@link Long }
                 */
                public void setVn(Long value) {
                    this.vn = value;
                }

                /**
                 * Obtient la valeur de la propriété spid.
                 *
                 * @return possible object is
                 * {@link String }
                 */
                public String getSPID() {
                    return spid;
                }

                /**
                 * Définit la valeur de la propriété spid.
                 *
                 * @param value allowed object is
                 *              {@link String }
                 */
                public void setSPID(String value) {
                    this.spid = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;choice&gt;
         *           &lt;sequence&gt;
         *             &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
         *             &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *               &lt;element name="notice" type="{http://www.ech.ch/xmlns/eCH-0214/2}noticeType"/&gt;
         *             &lt;/sequence&gt;
         *             &lt;choice&gt;
         *               &lt;element name="found"&gt;
         *                 &lt;complexType&gt;
         *                   &lt;complexContent&gt;
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                       &lt;sequence&gt;
         *                         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
         *                         &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
         *                       &lt;/sequence&gt;
         *                     &lt;/restriction&gt;
         *                   &lt;/complexContent&gt;
         *                 &lt;/complexType&gt;
         *               &lt;/element&gt;
         *               &lt;element name="maybeFound"&gt;
         *                 &lt;complexType&gt;
         *                   &lt;complexContent&gt;
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                       &lt;sequence&gt;
         *                         &lt;element name="candidate" maxOccurs="unbounded"&gt;
         *                           &lt;complexType&gt;
         *                             &lt;complexContent&gt;
         *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                 &lt;sequence&gt;
         *                                   &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
         *                                   &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
         *                                   &lt;element name="historicalValuesPersonFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" maxOccurs="unbounded" minOccurs="0"/&gt;
         *                                   &lt;element name="candidateLikeliness" type="{http://www.ech.ch/xmlns/eCH-0214/2}candidateLikelinessType" minOccurs="0"/&gt;
         *                                 &lt;/sequence&gt;
         *                               &lt;/restriction&gt;
         *                             &lt;/complexContent&gt;
         *                           &lt;/complexType&gt;
         *                         &lt;/element&gt;
         *                       &lt;/sequence&gt;
         *                     &lt;/restriction&gt;
         *                   &lt;/complexContent&gt;
         *                 &lt;/complexType&gt;
         *               &lt;/element&gt;
         *               &lt;element name="notFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *             &lt;/choice&gt;
         *           &lt;/sequence&gt;
         *           &lt;element name="negativReportOnSearchPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportTypeType"/&gt;
         *         &lt;/choice&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "searchPersonRequestId",
                "algorithm",
                "notice",
                "found",
                "maybeFound",
                "notFound",
                "negativReportOnSearchPerson"
        })
        public static class SearchPersonResponse {

            @XmlElement(required = true)
            protected BigInteger searchPersonRequestId;
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlSchemaType(name = "token")
            protected String algorithm;
            protected List<Notice> notice;
            protected Response.PositiveResponse.SearchPersonResponse.Found found;
            protected Response.PositiveResponse.SearchPersonResponse.MaybeFound maybeFound;
            protected Object notFound;
            protected NegativeReportType negativReportOnSearchPerson;

            /**
             * Obtient la valeur de la propriété searchPersonRequestId.
             *
             * @return possible object is
             * {@link BigInteger }
             */
            public BigInteger getSearchPersonRequestId() {
                return searchPersonRequestId;
            }

            /**
             * Définit la valeur de la propriété searchPersonRequestId.
             *
             * @param value allowed object is
             *              {@link BigInteger }
             */
            public void setSearchPersonRequestId(BigInteger value) {
                this.searchPersonRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété algorithm.
             *
             * @return possible object is
             * {@link String }
             */
            public String getAlgorithm() {
                return algorithm;
            }

            /**
             * Définit la valeur de la propriété algorithm.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setAlgorithm(String value) {
                this.algorithm = value;
            }

            /**
             * Gets the value of the notice property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the notice property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getNotice().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Notice }
             */
            public List<Notice> getNotice() {
                if (notice == null) {
                    notice = new ArrayList<Notice>();
                }
                return this.notice;
            }

            public void setNotice(List<Notice> notice) {
                this.notice = notice;
            }

            /**
             * Obtient la valeur de la propriété found.
             *
             * @return possible object is
             * {@link Response.PositiveResponse.SearchPersonResponse.Found }
             */
            public Response.PositiveResponse.SearchPersonResponse.Found getFound() {
                return found;
            }

            /**
             * Définit la valeur de la propriété found.
             *
             * @param value allowed object is
             *              {@link Response.PositiveResponse.SearchPersonResponse.Found }
             */
            public void setFound(Response.PositiveResponse.SearchPersonResponse.Found value) {
                this.found = value;
            }

            /**
             * Obtient la valeur de la propriété maybeFound.
             *
             * @return possible object is
             * {@link Response.PositiveResponse.SearchPersonResponse.MaybeFound }
             */
            public Response.PositiveResponse.SearchPersonResponse.MaybeFound getMaybeFound() {
                return maybeFound;
            }

            /**
             * Définit la valeur de la propriété maybeFound.
             *
             * @param value allowed object is
             *              {@link Response.PositiveResponse.SearchPersonResponse.MaybeFound }
             */
            public void setMaybeFound(Response.PositiveResponse.SearchPersonResponse.MaybeFound value) {
                this.maybeFound = value;
            }

            /**
             * Obtient la valeur de la propriété notFound.
             *
             * @return possible object is
             * {@link Object }
             */
            public Object getNotFound() {
                return notFound;
            }

            /**
             * Définit la valeur de la propriété notFound.
             *
             * @param value allowed object is
             *              {@link Object }
             */
            public void setNotFound(Object value) {
                this.notFound = value;
            }

            /**
             * Obtient la valeur de la propriété negativReportOnSearchPerson.
             *
             * @return possible object is
             * {@link NegativeReportType }
             */
            public NegativeReportType getNegativReportOnSearchPerson() {
                return negativReportOnSearchPerson;
            }

            /**
             * Définit la valeur de la propriété negativReportOnSearchPerson.
             *
             * @param value allowed object is
             *              {@link NegativeReportType }
             */
            public void setNegativReportOnSearchPerson(NegativeReportType value) {
                this.negativReportOnSearchPerson = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
             *         &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "pids",
                    "personFromUPI"
            })
            public static class Found {

                @XmlElement(required = true)
                protected PidsFromUPIType pids;
                @XmlElement(required = true)
                protected PersonFromUPIType personFromUPI;

                /**
                 * Obtient la valeur de la propriété pids.
                 *
                 * @return possible object is
                 * {@link PidsFromUPIType }
                 */
                public PidsFromUPIType getPids() {
                    return pids;
                }

                /**
                 * Définit la valeur de la propriété pids.
                 *
                 * @param value allowed object is
                 *              {@link PidsFromUPIType }
                 */
                public void setPids(PidsFromUPIType value) {
                    this.pids = value;
                }

                /**
                 * Obtient la valeur de la propriété personFromUPI.
                 *
                 * @return possible object is
                 * {@link PersonFromUPIType }
                 */
                public PersonFromUPIType getPersonFromUPI() {
                    return personFromUPI;
                }

                /**
                 * Définit la valeur de la propriété personFromUPI.
                 *
                 * @param value allowed object is
                 *              {@link PersonFromUPIType }
                 */
                public void setPersonFromUPI(PersonFromUPIType value) {
                    this.personFromUPI = value;
                }

            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="candidate" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
             *                   &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
             *                   &lt;element name="historicalValuesPersonFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" maxOccurs="unbounded" minOccurs="0"/&gt;
             *                   &lt;element name="candidateLikeliness" type="{http://www.ech.ch/xmlns/eCH-0214/2}candidateLikelinessType" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                    "candidate"
            })
            public static class MaybeFound {

                @XmlElement(required = true)
                protected List<Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate> candidate;

                /**
                 * Gets the value of the candidate property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the candidate property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getCandidate().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate }
                 */
                public List<Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate> getCandidate() {
                    if (candidate == null) {
                        candidate = new ArrayList<Response.PositiveResponse.SearchPersonResponse.MaybeFound.Candidate>();
                    }
                    return this.candidate;
                }

                public void setCandidate(List<Candidate> candidate) {
                    this.candidate = candidate;
                }

                /**
                 * <p>Classe Java pour anonymous complex type.
                 *
                 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
                 *         &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
                 *         &lt;element name="historicalValuesPersonFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType" maxOccurs="unbounded" minOccurs="0"/&gt;
                 *         &lt;element name="candidateLikeliness" type="{http://www.ech.ch/xmlns/eCH-0214/2}candidateLikelinessType" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                        "pids",
                        "personFromUPI",
                        "historicalValuesPersonFromUPI",
                        "candidateLikeliness"
                })
                public static class Candidate {

                    @XmlElement(required = true)
                    protected PidsFromUPIType pids;
                    @XmlElement(required = true)
                    protected PersonFromUPIType personFromUPI;
                    protected List<PersonFromUPIType> historicalValuesPersonFromUPI;
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger candidateLikeliness;

                    /**
                     * Obtient la valeur de la propriété pids.
                     *
                     * @return possible object is
                     * {@link PidsFromUPIType }
                     */
                    public PidsFromUPIType getPids() {
                        return pids;
                    }

                    /**
                     * Définit la valeur de la propriété pids.
                     *
                     * @param value allowed object is
                     *              {@link PidsFromUPIType }
                     */
                    public void setPids(PidsFromUPIType value) {
                        this.pids = value;
                    }

                    /**
                     * Obtient la valeur de la propriété personFromUPI.
                     *
                     * @return possible object is
                     * {@link PersonFromUPIType }
                     */
                    public PersonFromUPIType getPersonFromUPI() {
                        return personFromUPI;
                    }

                    /**
                     * Définit la valeur de la propriété personFromUPI.
                     *
                     * @param value allowed object is
                     *              {@link PersonFromUPIType }
                     */
                    public void setPersonFromUPI(PersonFromUPIType value) {
                        this.personFromUPI = value;
                    }

                    /**
                     * Gets the value of the historicalValuesPersonFromUPI property.
                     *
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the historicalValuesPersonFromUPI property.
                     *
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getHistoricalValuesPersonFromUPI().add(newItem);
                     * </pre>
                     *
                     *
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link PersonFromUPIType }
                     */
                    public List<PersonFromUPIType> getHistoricalValuesPersonFromUPI() {
                        if (historicalValuesPersonFromUPI == null) {
                            historicalValuesPersonFromUPI = new ArrayList<PersonFromUPIType>();
                        }
                        return this.historicalValuesPersonFromUPI;
                    }

                    /**
                     * Obtient la valeur de la propriété candidateLikeliness.
                     *
                     * @return possible object is
                     * {@link BigInteger }
                     */
                    public BigInteger getCandidateLikeliness() {
                        return candidateLikeliness;
                    }

                    /**
                     * Définit la valeur de la propriété candidateLikeliness.
                     *
                     * @param value allowed object is
                     *              {@link BigInteger }
                     */
                    public void setCandidateLikeliness(BigInteger value) {
                        this.candidateLikeliness = value;
                    }

                }

            }

        }

    }

}
