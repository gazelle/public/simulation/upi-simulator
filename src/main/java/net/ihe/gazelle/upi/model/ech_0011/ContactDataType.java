
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.ihe.gazelle.upi.model.ech_0010.MailAddressType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationLightType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationType;


/**
 * <p>Classe Java pour contactDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="contactDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
 *           &lt;element name="personIdentificationPartner" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationLightType"/&gt;
 *           &lt;element name="partnerIdOrganisation" type="{http://www.ech.ch/xmlns/eCH-0011/8}partnerIdOrganisationType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="contactAddress" type="{http://www.ech.ch/xmlns/eCH-0010/5}mailAddressType"/&gt;
 *         &lt;element name="contactValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="contactValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contactDataType", propOrder = {
    "personIdentification",
    "personIdentificationPartner",
    "partnerIdOrganisation",
    "contactAddress",
    "contactValidFrom",
    "contactValidTill"
})
public class ContactDataType {

    protected PersonIdentificationType personIdentification;
    protected PersonIdentificationLightType personIdentificationPartner;
    protected PartnerIdOrganisationType partnerIdOrganisation;
    @XmlElement(required = true)
    protected MailAddressType contactAddress;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar contactValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar contactValidTill;

    /**
     * Obtient la valeur de la propriété personIdentification.
     * 
     * @return
     *     possible object is
     *     {@link PersonIdentificationType }
     *     
     */
    public PersonIdentificationType getPersonIdentification() {
        return personIdentification;
    }

    /**
     * Définit la valeur de la propriété personIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonIdentificationType }
     *     
     */
    public void setPersonIdentification(PersonIdentificationType value) {
        this.personIdentification = value;
    }

    /**
     * Obtient la valeur de la propriété personIdentificationPartner.
     * 
     * @return
     *     possible object is
     *     {@link PersonIdentificationLightType }
     *     
     */
    public PersonIdentificationLightType getPersonIdentificationPartner() {
        return personIdentificationPartner;
    }

    /**
     * Définit la valeur de la propriété personIdentificationPartner.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonIdentificationLightType }
     *     
     */
    public void setPersonIdentificationPartner(PersonIdentificationLightType value) {
        this.personIdentificationPartner = value;
    }

    /**
     * Obtient la valeur de la propriété partnerIdOrganisation.
     * 
     * @return
     *     possible object is
     *     {@link PartnerIdOrganisationType }
     *     
     */
    public PartnerIdOrganisationType getPartnerIdOrganisation() {
        return partnerIdOrganisation;
    }

    /**
     * Définit la valeur de la propriété partnerIdOrganisation.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerIdOrganisationType }
     *     
     */
    public void setPartnerIdOrganisation(PartnerIdOrganisationType value) {
        this.partnerIdOrganisation = value;
    }

    /**
     * Obtient la valeur de la propriété contactAddress.
     * 
     * @return
     *     possible object is
     *     {@link MailAddressType }
     *     
     */
    public MailAddressType getContactAddress() {
        return contactAddress;
    }

    /**
     * Définit la valeur de la propriété contactAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MailAddressType }
     *     
     */
    public void setContactAddress(MailAddressType value) {
        this.contactAddress = value;
    }

    /**
     * Obtient la valeur de la propriété contactValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getContactValidFrom() {
        return contactValidFrom;
    }

    /**
     * Définit la valeur de la propriété contactValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setContactValidFrom(XMLGregorianCalendar value) {
        this.contactValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété contactValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getContactValidTill() {
        return contactValidTill;
    }

    /**
     * Définit la valeur de la propriété contactValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setContactValidTill(XMLGregorianCalendar value) {
        this.contactValidTill = value;
    }

}
