
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import net.ihe.gazelle.upi.model.ech_0010.AddressInformationType;


/**
 * <p>Classe Java pour occupationDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="occupationDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UID" type="{http://www.ech.ch/xmlns/eCH-0021/7}uidStructureType" minOccurs="0"/&gt;
 *         &lt;element name="employer" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="placeOfWork" type="{http://www.ech.ch/xmlns/eCH-0010/5}addressInformationType" minOccurs="0"/&gt;
 *         &lt;element name="placeOfEmployer" type="{http://www.ech.ch/xmlns/eCH-0010/5}addressInformationType" minOccurs="0"/&gt;
 *         &lt;element name="occupationValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="occupationValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "occupationDataType", propOrder = {
    "uid",
    "employer",
    "placeOfWork",
    "placeOfEmployer",
    "occupationValidFrom",
    "occupationValidTill"
})
public class OccupationDataType {

    @XmlElement(name = "UID")
    protected UidStructureType uid;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String employer;
    protected AddressInformationType placeOfWork;
    protected AddressInformationType placeOfEmployer;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar occupationValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar occupationValidTill;

    /**
     * Obtient la valeur de la propriété uid.
     * 
     * @return
     *     possible object is
     *     {@link UidStructureType }
     *     
     */
    public UidStructureType getUID() {
        return uid;
    }

    /**
     * Définit la valeur de la propriété uid.
     * 
     * @param value
     *     allowed object is
     *     {@link UidStructureType }
     *     
     */
    public void setUID(UidStructureType value) {
        this.uid = value;
    }

    /**
     * Obtient la valeur de la propriété employer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployer() {
        return employer;
    }

    /**
     * Définit la valeur de la propriété employer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployer(String value) {
        this.employer = value;
    }

    /**
     * Obtient la valeur de la propriété placeOfWork.
     * 
     * @return
     *     possible object is
     *     {@link AddressInformationType }
     *     
     */
    public AddressInformationType getPlaceOfWork() {
        return placeOfWork;
    }

    /**
     * Définit la valeur de la propriété placeOfWork.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInformationType }
     *     
     */
    public void setPlaceOfWork(AddressInformationType value) {
        this.placeOfWork = value;
    }

    /**
     * Obtient la valeur de la propriété placeOfEmployer.
     * 
     * @return
     *     possible object is
     *     {@link AddressInformationType }
     *     
     */
    public AddressInformationType getPlaceOfEmployer() {
        return placeOfEmployer;
    }

    /**
     * Définit la valeur de la propriété placeOfEmployer.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInformationType }
     *     
     */
    public void setPlaceOfEmployer(AddressInformationType value) {
        this.placeOfEmployer = value;
    }

    /**
     * Obtient la valeur de la propriété occupationValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOccupationValidFrom() {
        return occupationValidFrom;
    }

    /**
     * Définit la valeur de la propriété occupationValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOccupationValidFrom(XMLGregorianCalendar value) {
        this.occupationValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété occupationValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOccupationValidTill() {
        return occupationValidTill;
    }

    /**
     * Définit la valeur de la propriété occupationValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOccupationValidTill(XMLGregorianCalendar value) {
        this.occupationValidTill = value;
    }

}
