
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour armedForcesDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="armedForcesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="armedForcesService" type="{http://www.ech.ch/xmlns/eCH-0011/8}yesNoType" minOccurs="0"/&gt;
 *         &lt;element name="armedForcesLiability" type="{http://www.ech.ch/xmlns/eCH-0011/8}yesNoType" minOccurs="0"/&gt;
 *         &lt;element name="armedForcesValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "armedForcesDataType", propOrder = {
    "armedForcesService",
    "armedForcesLiability",
    "armedForcesValidFrom"
})
public class ArmedForcesDataType {

    protected String armedForcesService;
    protected String armedForcesLiability;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar armedForcesValidFrom;

    /**
     * Obtient la valeur de la propriété armedForcesService.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArmedForcesService() {
        return armedForcesService;
    }

    /**
     * Définit la valeur de la propriété armedForcesService.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArmedForcesService(String value) {
        this.armedForcesService = value;
    }

    /**
     * Obtient la valeur de la propriété armedForcesLiability.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArmedForcesLiability() {
        return armedForcesLiability;
    }

    /**
     * Définit la valeur de la propriété armedForcesLiability.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArmedForcesLiability(String value) {
        this.armedForcesLiability = value;
    }

    /**
     * Obtient la valeur de la propriété armedForcesValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getArmedForcesValidFrom() {
        return armedForcesValidFrom;
    }

    /**
     * Définit la valeur de la propriété armedForcesValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setArmedForcesValidFrom(XMLGregorianCalendar value) {
        this.armedForcesValidFrom = value;
    }

}
