
package net.ihe.gazelle.upi.model.ech_0006;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="residencePermitCategory" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitCategoryType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitRuling" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitRulingType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitBorder" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitBorderType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitShortType" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitShortType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermit" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitType" minOccurs="0"/&gt;
 *         &lt;element name="inhabitantControl" type="{http://www.ech.ch/xmlns/eCH-0006/2}inhabitantControlType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitDetailedType" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitDetailedType" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitToBeRegisteredType" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitToBeRegisteredType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "residencePermitCategory",
    "residencePermitRuling",
    "residencePermitBorder",
    "residencePermitShortType",
    "residencePermit",
    "inhabitantControl",
    "residencePermitDetailedType",
    "residencePermitToBeRegisteredType"
})
@XmlRootElement(name = "permitRoot")
public class PermitRoot {

    protected String residencePermitCategory;
    protected String residencePermitRuling;
    protected String residencePermitBorder;
    protected String residencePermitShortType;
    protected String residencePermit;
    protected String inhabitantControl;
    protected String residencePermitDetailedType;
    protected String residencePermitToBeRegisteredType;

    /**
     * Obtient la valeur de la propriété residencePermitCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitCategory() {
        return residencePermitCategory;
    }

    /**
     * Définit la valeur de la propriété residencePermitCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitCategory(String value) {
        this.residencePermitCategory = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitRuling.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitRuling() {
        return residencePermitRuling;
    }

    /**
     * Définit la valeur de la propriété residencePermitRuling.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitRuling(String value) {
        this.residencePermitRuling = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitBorder.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitBorder() {
        return residencePermitBorder;
    }

    /**
     * Définit la valeur de la propriété residencePermitBorder.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitBorder(String value) {
        this.residencePermitBorder = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitShortType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitShortType() {
        return residencePermitShortType;
    }

    /**
     * Définit la valeur de la propriété residencePermitShortType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitShortType(String value) {
        this.residencePermitShortType = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermit() {
        return residencePermit;
    }

    /**
     * Définit la valeur de la propriété residencePermit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermit(String value) {
        this.residencePermit = value;
    }

    /**
     * Obtient la valeur de la propriété inhabitantControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhabitantControl() {
        return inhabitantControl;
    }

    /**
     * Définit la valeur de la propriété inhabitantControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhabitantControl(String value) {
        this.inhabitantControl = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitDetailedType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitDetailedType() {
        return residencePermitDetailedType;
    }

    /**
     * Définit la valeur de la propriété residencePermitDetailedType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitDetailedType(String value) {
        this.residencePermitDetailedType = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitToBeRegisteredType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermitToBeRegisteredType() {
        return residencePermitToBeRegisteredType;
    }

    /**
     * Définit la valeur de la propriété residencePermitToBeRegisteredType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermitToBeRegisteredType(String value) {
        this.residencePermitToBeRegisteredType = value;
    }

}
