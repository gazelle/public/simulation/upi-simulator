
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour uidStructureType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="uidStructureType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uidOrganisationIdCategorie" type="{http://www.ech.ch/xmlns/eCH-0021/7}uidOrganisationIdCategorieType"/&gt;
 *         &lt;element name="uidOrganisationId" type="{http://www.ech.ch/xmlns/eCH-0021/7}uidOrganisationIdType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "uidStructureType", propOrder = {
    "uidOrganisationIdCategorie",
    "uidOrganisationId"
})
public class UidStructureType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected UidOrganisationIdCategorieType uidOrganisationIdCategorie;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected int uidOrganisationId;

    /**
     * Obtient la valeur de la propriété uidOrganisationIdCategorie.
     * 
     * @return
     *     possible object is
     *     {@link UidOrganisationIdCategorieType }
     *     
     */
    public UidOrganisationIdCategorieType getUidOrganisationIdCategorie() {
        return uidOrganisationIdCategorie;
    }

    /**
     * Définit la valeur de la propriété uidOrganisationIdCategorie.
     * 
     * @param value
     *     allowed object is
     *     {@link UidOrganisationIdCategorieType }
     *     
     */
    public void setUidOrganisationIdCategorie(UidOrganisationIdCategorieType value) {
        this.uidOrganisationIdCategorie = value;
    }

    /**
     * Obtient la valeur de la propriété uidOrganisationId.
     * 
     */
    public int getUidOrganisationId() {
        return uidOrganisationId;
    }

    /**
     * Définit la valeur de la propriété uidOrganisationId.
     * 
     */
    public void setUidOrganisationId(int value) {
        this.uidOrganisationId = value;
    }

}
