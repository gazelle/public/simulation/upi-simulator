
package net.ihe.gazelle.upi.model.ech_0011;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.ihe.gazelle.upi.model.ech_0008.CountryType;


/**
 * <p>Classe Java pour nationalityDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="nationalityDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nationalityStatus" type="{http://www.ech.ch/xmlns/eCH-0011/8}nationalityStatusType"/&gt;
 *         &lt;element name="countryInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="country" type="{http://www.ech.ch/xmlns/eCH-0008/3}countryType"/&gt;
 *                   &lt;element name="nationalityValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nationalityDataType", propOrder = {
    "nationalityStatus",
    "countryInfo"
})
public class NationalityDataType {

    @XmlElement(required = true)
    protected String nationalityStatus;
    protected List<NationalityDataType.CountryInfo> countryInfo;

    /**
     * Obtient la valeur de la propriété nationalityStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalityStatus() {
        return nationalityStatus;
    }

    /**
     * Définit la valeur de la propriété nationalityStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalityStatus(String value) {
        this.nationalityStatus = value;
    }

    /**
     * Gets the value of the countryInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the countryInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCountryInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NationalityDataType.CountryInfo }
     * 
     * 
     */
    public List<NationalityDataType.CountryInfo> getCountryInfo() {
        if (countryInfo == null) {
            countryInfo = new ArrayList<NationalityDataType.CountryInfo>();
        }
        return this.countryInfo;
    }

    public void setCountryInfo(List<CountryInfo> countryInfo) {
        this.countryInfo = countryInfo;
    }

    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="country" type="{http://www.ech.ch/xmlns/eCH-0008/3}countryType"/&gt;
     *         &lt;element name="nationalityValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "country",
        "nationalityValidFrom"
    })
    public static class CountryInfo {

        @XmlElement(required = true)
        protected CountryType country;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar nationalityValidFrom;

        /**
         * Obtient la valeur de la propriété country.
         * 
         * @return
         *     possible object is
         *     {@link CountryType }
         *     
         */
        public CountryType getCountry() {
            return country;
        }

        /**
         * Définit la valeur de la propriété country.
         * 
         * @param value
         *     allowed object is
         *     {@link CountryType }
         *     
         */
        public void setCountry(CountryType value) {
            this.country = value;
        }

        /**
         * Obtient la valeur de la propriété nationalityValidFrom.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getNationalityValidFrom() {
            return nationalityValidFrom;
        }

        /**
         * Définit la valeur de la propriété nationalityValidFrom.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setNationalityValidFrom(XMLGregorianCalendar value) {
            this.nationalityValidFrom = value;
        }

    }

}
