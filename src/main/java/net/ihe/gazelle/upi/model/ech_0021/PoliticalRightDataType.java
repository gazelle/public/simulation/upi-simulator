
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour politicalRightDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="politicalRightDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="restrictedVotingAndElectionRightFederation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "politicalRightDataType", propOrder = {
    "restrictedVotingAndElectionRightFederation"
})
public class PoliticalRightDataType {

    protected Boolean restrictedVotingAndElectionRightFederation;

    /**
     * Obtient la valeur de la propriété restrictedVotingAndElectionRightFederation.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictedVotingAndElectionRightFederation() {
        return restrictedVotingAndElectionRightFederation;
    }

    /**
     * Définit la valeur de la propriété restrictedVotingAndElectionRightFederation.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictedVotingAndElectionRightFederation(Boolean value) {
        this.restrictedVotingAndElectionRightFederation = value;
    }

}
