
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour civilDefenseDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="civilDefenseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="civilDefense" type="{http://www.ech.ch/xmlns/eCH-0011/8}yesNoType" minOccurs="0"/&gt;
 *         &lt;element name="civilDefenseValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "civilDefenseDataType", propOrder = {
    "civilDefense",
    "civilDefenseValidFrom"
})
public class CivilDefenseDataType {

    protected String civilDefense;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar civilDefenseValidFrom;

    /**
     * Obtient la valeur de la propriété civilDefense.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCivilDefense() {
        return civilDefense;
    }

    /**
     * Définit la valeur de la propriété civilDefense.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCivilDefense(String value) {
        this.civilDefense = value;
    }

    /**
     * Obtient la valeur de la propriété civilDefenseValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCivilDefenseValidFrom() {
        return civilDefenseValidFrom;
    }

    /**
     * Définit la valeur de la propriété civilDefenseValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCivilDefenseValidFrom(XMLGregorianCalendar value) {
        this.civilDefenseValidFrom = value;
    }

}
