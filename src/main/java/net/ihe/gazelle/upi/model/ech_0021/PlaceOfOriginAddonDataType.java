
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour placeOfOriginAddonDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="placeOfOriginAddonDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="naturalizationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="expatriationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "placeOfOriginAddonDataType", propOrder = {
    "naturalizationDate",
    "expatriationDate"
})
@XmlSeeAlso({
    PlaceOfOriginAddonRestrictedNaturalizeDataType.class,
    PlaceOfOriginAddonRestrictedUnDoDataType.class
})
public class PlaceOfOriginAddonDataType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar naturalizationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expatriationDate;

    /**
     * Obtient la valeur de la propriété naturalizationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNaturalizationDate() {
        return naturalizationDate;
    }

    /**
     * Définit la valeur de la propriété naturalizationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNaturalizationDate(XMLGregorianCalendar value) {
        this.naturalizationDate = value;
    }

    /**
     * Obtient la valeur de la propriété expatriationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpatriationDate() {
        return expatriationDate;
    }

    /**
     * Définit la valeur de la propriété expatriationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpatriationDate(XMLGregorianCalendar value) {
        this.expatriationDate = value;
    }

}
