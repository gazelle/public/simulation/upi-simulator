
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour maritalDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="maritalDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maritalStatus" type="{http://www.ech.ch/xmlns/eCH-0011/8}maritalStatusType"/&gt;
 *         &lt;element name="dateOfMaritalStatus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="cancelationReason" type="{http://www.ech.ch/xmlns/eCH-0011/8}partnershipAbolitionType" minOccurs="0"/&gt;
 *         &lt;element name="officialProofOfMaritalStatusYesNo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="separationData" type="{http://www.ech.ch/xmlns/eCH-0011/8}separationDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maritalDataType", propOrder = {
    "maritalStatus",
    "dateOfMaritalStatus",
    "cancelationReason",
    "officialProofOfMaritalStatusYesNo",
    "separationData"
})
@XmlSeeAlso({
    MaritalDataRestrictedMarriageType.class,
    MaritalDataRestrictedPartnershipType.class,
    MaritalDataRestrictedDivorceType.class,
    MaritalDataRestrictedUndoMarriedType.class,
    MaritalDataRestrictedUndoPartnershipType.class,
    MaritalDataRestrictedMaritalStatusPartnerType.class
})
public class MaritalDataType {

    @XmlElement(required = true)
    protected String maritalStatus;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfMaritalStatus;
    protected String cancelationReason;
    protected Boolean officialProofOfMaritalStatusYesNo;
    protected SeparationDataType separationData;

    /**
     * Obtient la valeur de la propriété maritalStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Définit la valeur de la propriété maritalStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaritalStatus(String value) {
        this.maritalStatus = value;
    }

    /**
     * Obtient la valeur de la propriété dateOfMaritalStatus.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfMaritalStatus() {
        return dateOfMaritalStatus;
    }

    /**
     * Définit la valeur de la propriété dateOfMaritalStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfMaritalStatus(XMLGregorianCalendar value) {
        this.dateOfMaritalStatus = value;
    }

    /**
     * Obtient la valeur de la propriété cancelationReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelationReason() {
        return cancelationReason;
    }

    /**
     * Définit la valeur de la propriété cancelationReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelationReason(String value) {
        this.cancelationReason = value;
    }

    /**
     * Obtient la valeur de la propriété officialProofOfMaritalStatusYesNo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOfficialProofOfMaritalStatusYesNo() {
        return officialProofOfMaritalStatusYesNo;
    }

    /**
     * Définit la valeur de la propriété officialProofOfMaritalStatusYesNo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOfficialProofOfMaritalStatusYesNo(Boolean value) {
        this.officialProofOfMaritalStatusYesNo = value;
    }

    /**
     * Obtient la valeur de la propriété separationData.
     * 
     * @return
     *     possible object is
     *     {@link SeparationDataType }
     *     
     */
    public SeparationDataType getSeparationData() {
        return separationData;
    }

    /**
     * Définit la valeur de la propriété separationData.
     * 
     * @param value
     *     allowed object is
     *     {@link SeparationDataType }
     *     
     */
    public void setSeparationData(SeparationDataType value) {
        this.separationData = value;
    }

}
