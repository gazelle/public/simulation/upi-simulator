package ws.ech_0214.examples.examples;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

import javax.xml.bind.annotation.XmlSeeAlso;
import net.ihe.gazelle.upi.model.ech_0213_commons.ObjectFactory;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;

/**
 * This class was generated by Apache CXF 3.5.0
 * 2022-01-14T15:39:05.507+01:00
 * Generated source version: 3.5.0
 *
 */
@WebService(targetNamespace = "http://www.zas.admin.ch/wupispid/ws/queryService/2", name = "SpidQueryServicePortTypeV2")
@XmlSeeAlso({net.ihe.gazelle.upi.model.ech_0008.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0058.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0010.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0011.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0006.ObjectFactory.class, ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0044.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0214.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0135.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0021.ObjectFactory.class, net.ihe.gazelle.upi.model.ech_0007.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface SpidQueryServicePortTypeV2 {

    @WebMethod(action = "http://www.zas.admin.ch")
    @WebResult(name = "response", targetNamespace = "http://www.ech.ch/xmlns/eCH-0214/2", partName = "body")
    public Response querySpid(

        @WebParam(partName = "body", name = "request", targetNamespace = "http://www.ech.ch/xmlns/eCH-0214/2")
                Request body
    );

}
