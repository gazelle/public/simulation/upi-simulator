<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/3"
           xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/1"
           xmlns:eCH-0072="http://www.ech.ch/xmlns/eCH-0072/1"
           xmlns:eCH-0084="http://www.ech.ch/xmlns/eCH-0084/1"
           xmlns:eCH-0086="http://www.ech.ch/xmlns/eCH-0086/1"
           targetNamespace="http://www.ech.ch/xmlns/eCH-0086/1"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified"
           version="1">

  <xs:annotation>
    <xs:documentation xml:lang="en">
      UPI (unique person identifier) compare interface.
      Schema for requests and responses for UPI related bulk comparison messages.

      $Id: eCH-0086-1-1.xsd,v 1.2 2009-12-10 16:00:24 dbn Exp $
    </xs:documentation>
  </xs:annotation>

  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0011/3" schemaLocation="eCH-0011-3-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0044/1" schemaLocation="eCH-0044-1-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0072/1" schemaLocation="eCH-0072-1-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0084/1" schemaLocation="eCH-0084-commons-1-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0090/1" schemaLocation="eCH-0090-1-0.xsd"/>

  <xs:complexType name="refusedType">
    <xs:annotation>
      <xs:documentation xml:lang="en">
        Type of element, which indicates errors in the request.
      </xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="reason">
        <xs:annotation>
          <xs:documentation xml:lang="en">
            The reason for refusing is: 1 = not valid against XSD or an other formal reason (wrong control
            number of ahvn for instance), or 3 = ahvvn not found, or 4 ahvvn no more valid, or 5 = maximum
            amount of records for request exceeded, or 10 = service not available.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:short">
            <xs:enumeration value="1"/>
            <xs:enumeration value="3"/>
            <xs:enumeration value="4"/>
            <xs:enumeration value="5"/>
            <xs:enumeration value="10"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:element>
      <xs:element name="detailedReason" minOccurs="0">
        <xs:annotation>
          <xs:documentation xml:lang="en">Starts with the detailed error code followed by a short standard text.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="50"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>

  <xs:element name="compareDataRequest">
		<xs:annotation>
		  <xs:documentation xml:lang="en">Contains the identification attributs of a person which have to be compared against the attributs of the UPI</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="exportDate" type="xs:date">
				  <xs:annotation>
				    <xs:documentation xml:lang="en">Point in time, when the data to compare has been exported from the data source.</xs:documentation>
				  </xs:annotation>
				</xs:element>
				<xs:element name="dataToCompare" maxOccurs="unbounded">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="ahvvn" type="eCH-0044:vnType"/>
							<xs:element name="firstNames" type="eCH-0084:baseNameUPI_Type"/>
							<xs:element name="officialName" type="eCH-0084:baseNameUPI_Type"/>
							<xs:element name="originalName" type="eCH-0084:baseNameUPI_Type" minOccurs="0"/>
							<xs:element name="sex" type="eCH-0044:sexType"/>
						  <xs:element name="dateOfBirth" type="eCH-0044:datePartiallyKnownType"/>
							<xs:element name="nationality">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="nationalityStatus" type="eCH-0011:nationalityStatusType"/>
										<xs:element name="countryId" type="eCH-0072:countryIdType" minOccurs="0"/>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						  <xs:element name="placeOfBirth" type="eCH-0084:placeOfBirthType" minOccurs="0">
								<xs:annotation>
									<xs:documentation xml:lang="en">
										For the case of a birth in a Swiss town, the comparison considers that the data are matching if one of the following comparisons matches: 1) historyMunicipalityId, 2) current municipality name, 3) historical municipality name. If available historyMunicipalityId should be sent.
									</xs:documentation>
								</xs:annotation>
							</xs:element>
							<xs:element name="mothersName" type="eCH-0084:fullName_Type" minOccurs="0"/>
							<xs:element name="fathersName" type="eCH-0084:fullName_Type" minOccurs="0"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

  <xs:element name="compareDataResponse">
    <xs:annotation>
      <xs:documentation xml:lang="en">
        Response to UPI related data comparison. Only the field which have been sent are compared.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:choice>
        <xs:element name="refused" type="eCH-0086:refusedType">
          <xs:annotation>
            <xs:documentation xml:lang="en">
              Is present, if the request did not comply with the required XML Schema.
            </xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:sequence>
          <xs:element name="comparedData" maxOccurs="unbounded">
            <xs:complexType>
              <xs:sequence>
                <xs:element name="timestamp" type="xs:dateTime">
                  <xs:annotation>
                    <xs:documentation xml:lang="en">The time, when the response was generated (for online processing). The time, when the download was generated (for batch processing based on a download).</xs:documentation>
                  </xs:annotation>
                </xs:element>
                <xs:element name="ahvvn" type="eCH-0044:vnType"/>
                <xs:choice>
                  <xs:element name="identicalData">
                    <xs:annotation>
                      <xs:documentation>This element is used to indicate, that all the sent data are the same for the person identified by ahvvn.</xs:documentation>
                    </xs:annotation>
                  </xs:element>
                  <xs:element name="differentData">
                    <xs:annotation>
                      <xs:documentation>At least one sent field is different from the UPI content. Only the field which have been sent are returned.</xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                      <xs:sequence>
                        <xs:element name="latestAhvn" type="eCH-0044:vnType"/>
                        <xs:element name="valuesStoredUnderAhvvn" type="eCH-0084:valuesStoredUnderAhvvn_Type">
                          <xs:annotation>
                            <xs:documentation xml:lang="en">This element  contains the current values (recorded in the UPI at the moment mentioned in the element called timestamp) of the attributes of the person having the AHVN contained in the element called ahvn. </xs:documentation>
                          </xs:annotation>
                        </xs:element>
                      </xs:sequence>
                    </xs:complexType>
                  </xs:element>
                  <xs:element name="refused" type="eCH-0086:refusedType">
                    <xs:annotation>
                      <xs:documentation xml:lang="en">
                        Is present, if the request contained an error and has not been processed normally.
                      </xs:documentation>
                    </xs:annotation>
                  </xs:element>
                </xs:choice>
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:sequence>
      </xs:choice>
    </xs:complexType>
  </xs:element>

</xs:schema>
