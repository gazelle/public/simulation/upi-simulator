package net.ihe.gazelle.upi.ws.eCH_0213_WS;

import io.qameta.allure.Description;
import net.ihe.gazelle.upi.ech_0213.business.ResponseManager;
import net.ihe.gazelle.upi.mock.eCH_0213.RequestMock;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.model.ech_0213.Response;
import net.ihe.gazelle.upi.ech_0213.business.UPISedexManagerDispatcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;


class ECH0213WSTest {

    @Test
    @Description("check a total routing from Generate Request to Return a Response with excepted SPID")
    void manageSPIDGenerateTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_generate_SPID.xml");
        Response r = responseManager.createResponse(reqXML);
        String actualSPID = r.getPositiveResponse().getPids().getSPID().get(0);
        Assertions.assertEquals("735757560520358416", actualSPID);
    }

    @Test
    @Description("check a total routing from Generate Request to Return a Response with excepted SPID")
    void manageSPIDGenerateWithWarningTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_generate_warning_SPID.xml");
        Response r = responseManager.createResponse(reqXML);
        int actualCode = r.getPositiveResponse().getWarning().get(0).getCode();
        Assertions.assertEquals(210401, actualCode);
    }

    @Test
    @Description("check if it returns an IllegalArgumentException with the right message with an unauthorirized action on SPID")
    void manageSPIDIncorrectActionTest() {
        ResponseManager responseManager = new ResponseManager();
        Request request = new Request();
        request.setContent(new Request.Content());
        request.getContent().setActionOnSPID("foobar");
        Response r = responseManager.createResponse(request);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(300501, noticeCode);
    }

    @Test
    @Description("return a negative report from Generate if firstName is blank or null")
    void generateNegativeReportWithNullFirstNameTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_generate_SPID.xml");
        reqXML.getContent().getPersonToUPI().setFirstName(null);
        Response r = responseManager.createResponse(reqXML);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(300001, noticeCode);
    }

    @Test
    @Description("return a negative report from Generate if officialName is blank or null")
    void generateNegativeReportWithNullOfficialNameTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_generate_SPID.xml");
        reqXML.getContent().getPersonToUPI().setOfficialName(null);
        Response r = responseManager.createResponse(reqXML);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(300001, noticeCode);
    }

    @Test
    @Description("return a negative report from Generate if birthDay is blank or null")
    void generateNegativeReportWithNullBirthdayTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_generate_SPID.xml");
        reqXML.getContent().getPersonToUPI().getDateOfBirth().setYearMonthDay(null);
        Response r = responseManager.createResponse(reqXML);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(300001, noticeCode);
    }

    @Test
    @Description("check if it returns the right answer with a inactivate action on SPID")
    void manageSPIDInactivateTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_inactivate_SPID.xml");
        Response r = responseManager.createResponse(reqXML);
        Assertions.assertEquals(1,r.getPositiveResponse().getPids().getSPID().size());
    }

    @Test
    @Description("check if it returns the right answer with a inactivate action on SPID")
    void manageSPIDInactivateWrongSpidTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_inactivate_wrong_SPID.xml");
        Response r = responseManager.createResponse(reqXML);
        Assertions.assertEquals(2,r.getPositiveResponse().getPids().getSPID().size());
    }

    @Test
    @Description("return a negative report from inactivate if SPID to keep is blank or null")
    void inactivateNegativeReportWithOneSPIDTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_inactivate_SPID.xml");
        reqXML.getContent().getPidsToUPI().get(1).getContent().get(0).setValue(null);
        Response r = responseManager.createResponse(reqXML);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(307101, noticeCode);
    }

    @Test
    @Description("check if it returns the right answer with a inactivate action on SPID")
    void manageSPIDCancelTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_cancel_SPID.xml");
        Response r = responseManager.createResponse(reqXML);
        Assertions.assertEquals(2,r.getPositiveResponse().getPids().getSPID().size());
    }

    @Test
    @Description("check if it returns the right answer with a inactivate action on SPID")
    void manageSPIDFailedCancelTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_cancel_wrong_SPID.xml");
        Assertions.assertThrows(NullPointerException.class, ()->responseManager.createResponse(reqXML));
    }

    @Test
    @Description("check if it returns the right answer with a inactivate action on SPID")
    void missingSPIDCancelTest() {
        ResponseManager responseManager = new ResponseManager();
        RequestMock requestMock = new RequestMock();
        Request reqXML = requestMock.instantiateRequestFromXML("src/test/resources/soapui/eCH_0213/request/eCH_0213_request_cancel_SPID.xml");
        reqXML.getContent().getPidsToUPI().get(0).getContent().get(0).setValue(null);
        Response r = responseManager.createResponse(reqXML);
        int noticeCode = r.getNegativeReport().getNotice().getCode();
        Assertions.assertEquals(307101, noticeCode);
    }


    @Test
    @Description("check if it returns an NullPointerException with the right message if Request is null ")
    void requestIsNull() throws NullPointerException {
        UPISedexManagerDispatcher upsd = new UPISedexManagerDispatcher();
        NullPointerException npe = Assertions.assertThrows(NullPointerException.class, () -> upsd.actionOnSPIDToMap(null));
        Assertions.assertEquals("eCH-0213 Request from SOAP is null", npe.getMessage());
    }

    @Test
    @Description("check if it returns an Exception if Request is null")
    void requestContentIsNull() throws NullPointerException {
        Request req = new Request();
        req.setContent(null);
        UPISedexManagerDispatcher upsd = new UPISedexManagerDispatcher();
        NullPointerException npe = Assertions.assertThrows(NullPointerException.class, () -> upsd.actionOnSPIDToMap(req));
        Assertions.assertEquals("ech-0123 Request.Content from SOAP is null", npe.getMessage());
    }


}

